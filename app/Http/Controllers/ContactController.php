<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;

class ContactController extends Controller
{
	public function send(Request $request)
	{
		// Validacia 
		if ($request->type == 'demo') {
			$rules = [
				'demo_name' => ['required'],
				'demo_email' => ['email'],
				'demo_company' => ['required'],
				'demo_date' => ['required'],
				'demo_notes' => ['required'],
				'demo_agree' => ['required'],
				'g-recaptcha-response' => ['grecaptcha']
			];
		} else {
			$rules = [
				'expert_name' => ['required'],
				'expert_email' => ['email'],
				'expert_company' => ['required'],
				'expert_project_size' => ['required'],
				'expert_owner' => ['required'],
				'expert_project_details' => ['required'],
				'expert_agree' => ['required'],
				'g-recaptcha-response'  => ['grecaptcha']
			];
		}

		$request->validate($rules);

		// Poslanie emailu pouzivatelovi
		Mail::to('leon@wearepenpaper.com')->send(new ContactMail($request));

		// Vratenie response
		return response()->json([
			'status' => 200
		]);
	}
}
