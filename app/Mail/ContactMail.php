<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The request instance.
     *
     * @var Request
     */
    protected $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		if ($this->request->type == 'demo') {
			return $this->subject('Request a demo')
						->markdown('emails.contact')
						->with([
							'type' => $this->request->type,
							'name' => $this->request->demo_name,
							'email' => $this->request->demo_email,
							'company' => $this->request->demo_company,
							'date' => $this->request->demo_date,
							'notes' => $this->request->demo_notes,
						]);
		} else {
			return $this->subject('Brief an expert')
						->markdown('emails.contact')
						->with([
							'type' => $this->request->type,
							'name' => $this->request->expert_name,
							'email' => $this->request->expert_email,
							'company' => $this->request->expert_company,
							'size' => $this->request->expert_project_size,
							'owner' => $this->request->expert_owner,
							'details' => $this->request->expert_project_details,
						]);
		}
    }
}