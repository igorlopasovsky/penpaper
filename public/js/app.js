/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/***/ (() => {

$(function () {
  /*
   * Povolit scroll
   */
  function enableScroll() {
    $('html, body').removeClass('overflow-y-hidden');
  }
  /*
   * Zakazat scroll
   */


  function disableScroll() {
    $('html, body').addClass('overflow-y-hidden');
  }
  /*
   * Skrolovanie
   */


  function scrolled() {
    if ($(window).scrollTop() > 0) {
      $('.header').addClass('bg-gray-300');
      $('.header').children().first().removeClass('h-28').addClass('h-20');
      $('.navBtn').removeClass('bg-btn-blue hover:bg-btn-blue-hover').addClass('border-2 border-white rounded-full transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple');
    } else {
      $('.header').removeClass('bg-gray-300');
      $('.header').children().first().removeClass('h-20').addClass('h-28');
      $('.navBtn').removeClass('border-2 border-white rounded-full transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple').addClass('bg-btn-blue hover:bg-btn-blue-hover');
    }
  }

  scrolled();
  $(window).on('scroll', function (event) {
    scrolled();
  });
  /*
   * Mobilne menu
   */

  $('.navOpen').on('click', function (event) {
    disableScroll();
    $('.navMenu').removeClass('2xl:translate-x-full').addClass('2xl:translate-x-0');
  });
  $('.navClose').on('click', function (event) {
    enableScroll();
    $('.navMenu').removeClass('2xl:translate-x-0').addClass('2xl:translate-x-full');
  });
  /*
   * Animovany scroll
   */

  $('.animateScroll').on('click', function (event) {
    event.preventDefault();
    $('html, body').animate({
      scrollTop: $($(this).attr('href')).offset().top - $('.header').outerHeight()
    }, 1200);

    if (window.matchMedia('(max-width: 1399.98px)').matches) {
      $('.navClose').trigger('click');
    }
  });
  /*
   * Plus odkaz
   */

  $('.plusBtn').on('mouseenter', function (event) {
    $(this).removeClass('text-opacity-60').addClass('text-opacity-100');
    $(this).find('.plusBtnIcon').removeClass('opacity-0').addClass('opacity-100');
  });
  $('.plusBtn').on('mouseleave', function (event) {
    if (!$(this).hasClass('plusBtnActive')) {
      $(this).removeClass('text-opacity-100').addClass('text-opacity-60');
      $(this).find('.plusBtnIcon').removeClass('opacity-100').addClass('opacity-0');
    }
  });
  $('.plusBtn').on('click', function (event) {
    $(this).parents('.plus').siblings().each(function (index, item) {
      $(item).find('.plusBtn').removeClass('plusBtnActive');
      $(item).find('.plusBtn').removeClass('text-opacity-100').addClass('text-opacity-60');
      $(item).find('.plusBtnIcon').removeClass('opacity-100').addClass('opacity-0');
      $(item).find('.plusBtnIconLine').fadeIn(100);
      $(item).find('.plusMain').slideUp(100);
    });
    $(this).toggleClass('plusBtnActive');

    if ($(this).hasClass('plusBtnActive')) {
      $(this).next('.plusMain').slideDown(100);
      $(this).removeClass('text-opacity-60').addClass('text-opacity-100');
      $(this).find('.plusBtnIcon').removeClass('opacity-0').addClass('opacity-100');
      $(this).find('.plusBtnIconLine').fadeOut(100);
    } else {
      $(this).next('.plusMain').slideUp(100);
      $(this).find('.plusBtnIconLine').fadeIn(100);
    }
  });
  /*
   * Logo slider
   */

  if ($('.logoSliderSlides').length) {
    var logoSlider = tns({
      container: '.logoSliderSlides',
      mouseDrag: true,
      loop: true,
      nav: false,
      controls: false,
      autoplayTimeout: 2000,
      responsive: {
        0: {
          items: 2,
          autoplay: true
        },
        576: {
          items: 3,
          autoplay: true
        },
        767: {
          items: 4,
          edgePadding: 75,
          autoplay: true
        },
        992: {
          items: 5,
          edgePadding: 100,
          autoplay: true
        },
        1200: {
          items: 7,
          autoplay: false
        }
      }
    });
  }
  /*
   * Case study slider
   */


  if ($('.caseSliderSlides').length) {
    var caseSlider = tns({
      container: '.caseSliderSlides',
      items: 1,
      mouseDrag: false,
      loop: true,
      nav: false,
      controls: false,
      autoplay: true,
      autoplayTimeout: 10000
    }); // Reset timer pri draggovani

    caseSlider.events.on('dragStart', function () {
      caseSlider.pause();
    });
    caseSlider.events.on('dragMove', function () {
      caseSlider.pause();
    });
    caseSlider.events.on('dragEnd', function () {
      caseSlider.pause();
    }); // Predchadzajuci slide

    $('.caseSliderPrev').on('click', function (event) {
      caseSlider.goTo('prev');
    }); // Nasledujuci slide

    $('.caseSliderNext').on('click', function (event) {
      caseSlider.goTo('next');
    }); // Otvorenie detailu

    $('.caseBtn').on('click', function (event) {
      event.preventDefault();
      caseSlider.pause();
      $($(this).attr('href')).fadeIn(100);

      if (window.matchMedia('(min-width: 992px)').matches) {
        $('html, body').animate({
          scrollTop: $('#case-studies').offset().top - $('.header').outerHeight()
        }, 300);
      }

      if (window.matchMedia('(max-width: 991.98px)').matches) {
        disableScroll();
      }

      $('.caseSliderBtns').addClass('hidden');
      $('.caseSliderPrev, .caseSliderNext').removeClass('lg:block');
    }); // Zatvorenie detailu

    $('.caseClose').on('click', function (event) {
      caseSlider.play();
      $(this).parents('.caseDetail').fadeOut(100);

      if (window.matchMedia('(max-width: 991.98px)').matches) {
        enableScroll();
      }

      $('.caseSliderBtns').removeClass('hidden');
      $('.caseSliderPrev, .caseSliderNext').addClass('lg:block');
    });
  }
  /*
   * Referencny slider
   */


  if ($('.refSliderSlides').length) {
    var refSlider = tns({
      container: '.refSliderSlides',
      items: 1,
      mouseDrag: true,
      loop: true,
      nav: false,
      controls: false,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true
    });
    $('.refSliderPrev').on('click', function (event) {
      refSlider.goTo('prev');
    }); // Nasledujuci slide

    $('.refSliderNext').on('click', function (event) {
      refSlider.goTo('next');
    });
  }
  /*
   * Zobrazenie formulara
   */


  $('.showFormBtn').on('click', function (event) {
    event.preventDefault();
    $(this).toggleClass('showFormBtnActive');

    if ($(this).hasClass('showFormBtnActive')) {
      $(this).removeClass('text-black').addClass('text-white bg-black');
      $($(this).attr('href')).slideDown(100);
      $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top - $('.header').outerHeight()
      }, 300);
    } else {
      $(this).removeClass('text-white bg-black').addClass('text-black');
      $($(this).attr('href')).slideUp(100);
    }
  });
  /*
   * Odoslanie formulara
   */

  $('.form').on('submit', function (event) {
    event.preventDefault();
    var form = $(this);
    $.ajax({
      method: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize(),
      beforeSend: function beforeSend() {
        form.find('input[type="text"]').removeClass('border-red').addClass('border-black');
        form.find('input[type="email"]').removeClass('border-red').addClass('border-black');
        form.find('textarea').removeClass('border-red').addClass('border-black');
        form.find('input[type="checkbox"]').removeClass('border-red').addClass('border-black').next('label').removeClass('text-red');
        form.find('.formError').fadeOut(100);
      },
      error: function error(data) {
        $.each(data.responseJSON.errors, function (key, value) {
          if (key) {
            if (key == 'demo_agree' || key == 'expert_agree') {
              form.find('input[type="checkbox"]').removeClass('border-black').addClass('border-red').next('label').addClass('text-red');
            } else {
              form.find('#' + key).removeClass('border-black').addClass('border-red').next('.formError').fadeIn(100).html(value.toString().split(',')[0]);
            }
          }
        });
      },
      success: function success(data) {
        var height = form.outerHeight();
        form[0].reset();
        form.parent('.showFormBox').replaceWith('<div class="flex justify-center items-center text-5xl font-bold leading-tight uppercase text-center text-black" style="height: ' + height + 'px">Thanks, we\'ll get right back <br>to you with expert <br>recommendations.</div>');
      },
      complete: function complete(data) {
        grecaptcha.execute('6LeDTGsdAAAAALW5Y30xR3Oow5mU4GCSu86sQsJj', {
          action: 'form'
        }).then(function (token) {
          var recaptchaResponse = document.getElementsByClassName('g-recaptcha-response');

          for (i = 0; i < recaptchaResponse.length; i++) {
            recaptchaResponse[i].value = token;
          }
        });
      }
    });
    return false;
  });
  /*
   * Cookies
   */

  function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + 120 * 24 * 60 * 60 * 1000);
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
  }

  function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
  }

  if (getCookie(window.location.hostname + '_main_cookie') == null) {
    $('.cookiesBox').fadeIn(200);
  }

  $('.cookiesClose').on('click', function (event) {
    setCookie(window.location.hostname + '_main_cookie', 'yes');
    $('.cookiesBox').fadeOut(200);
  });

  if (getCookie(window.location.hostname + '_notice_cookie') == null) {
    $('.noticeBox').fadeIn(200);
  }

  $('.noticeClose').on('click', function (event) {
    setCookie(window.location.hostname + '_notice_cookie', 'yes');
    $('.noticeBox').fadeOut(200);
  });
});

/***/ }),

/***/ "./resources/css/app.css":
/*!*******************************!*\
  !*** ./resources/css/app.css ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/js/app": 0,
/******/ 			"css/app": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["css/app"], () => (__webpack_require__("./resources/js/app.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["css/app"], () => (__webpack_require__("./resources/css/app.css")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;