@component('mail::message')

@if ($type == 'demo')
# Request a demo 

**Name:** {{ $name }}  
**Email:** {{ $email }}  
**Company:** {{ $company }}  
**Preffered date and time:** {{ $date }}  
**Anything to add:** {{ $notes }}
@else 
# Brief an expert

**Name:** {{ $name }}  
**Email:** {{ $email }}  
**Company:** {{ $company }}  
**Project size:** {{ $size }}  
**Owner:** {{ $owner }}  
**Project details:** {{ $details }}
@endif

@endcomponent
