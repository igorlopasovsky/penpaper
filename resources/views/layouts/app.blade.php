<!doctype html>
<html lang="{{ app()->getLocale() }}" class="overflow-x-hidden">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8"/>
    <meta name="google" content="notranslate">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, viewport-fit=cover">
    <meta name="author" content="broccoli.design">
    <title>Pen & Paper - Hand-Picked Marketing Freelancers</title>
    <meta name="description" content="Central Europe's network of proven & tried advertising freelancers with agency experience, outstanding communication skills, and conscientious work ethic.">

    <meta property="og:title" content="Pen & Paper - Hand-Picked Marketing Freelancers" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image" content="{{ asset('images/og.gif') }}" />
    <meta property="og:image:alt" content="Pen & Paper - Hand-Picked Marketing Freelancers" />
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="450" />
    <meta property="og:description" content="Central Europe's network of proven & tried advertising freelancers with agency experience, outstanding communication skills, and conscientious work ethic.r" />
    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="">

	<meta name="twitter:card" content="summary_large_image">
	<meta property="twitter:domain" content="{{ request()->getHost() }}">
	<meta property="twitter:url" content="{{ url()->current() }}">
	<meta name="twitter:title" content="Pen & Paper - Hand-Picked Marketing Freelancers">
	<meta name="twitter:description" content="Central Europe's network of proven & tried advertising freelancers with agency experience, outstanding communication skills, and conscientious work ethic.">
	<meta name="twitter:image" content="{{ asset('images/og.gif') }}">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/favicon/apple-icon-57x57.png') }}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/favicon/apple-icon-60x60.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/favicon/apple-icon-72x72.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/favicon/apple-icon-76x76.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/favicon/apple-icon-114x114.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/favicon/apple-icon-120x120.png') }}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/favicon/apple-icon-144x144.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/favicon/apple-icon-152x152.png') }}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon/apple-icon-180x180.png') }}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/favicon/android-icon-192x192.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicon/favicon-96x96.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('images/favicon/manifest.json') }}">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{ asset('images/favicon/ms-icon-144x144.png') }}">
	<meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/vendor.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/app.css')) }}">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-H3RYWP95G0"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-H3RYWP95G0');
	</script>
</head>
<body class="font-wotfard antialiased bg-gray-200 overflow-x-hidden">
	<header class="fixed top-0 left-0 right-0 transition-colors duration-300 z-40 header">
		<div class="flex justify-between items-center w-full h-28 px-12 transition-all duration-300 lg:px-4 sm:h-16">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 233.21 32.34" class="w-48 h-7 fill-current text-white 2xl:z-50">
				<path d="M8.66.65H.1V17.07h3V11H8.66C12,11,14.27,8.7,14.27,5.82S12,.65,8.66.65ZM8.27,8.41H3.13V3.34H8.27a2.61,2.61,0,0,1,2.88,2.53A2.61,2.61,0,0,1,8.27,8.41Z"/>
				<polygon points="23.8 17.07 36.14 17.07 36.14 14.39 26.83 14.39 26.83 10.17 35.75 10.17 35.75 7.55 26.83 7.55 26.83 3.39 36.14 3.39 36.14 0.65 23.8 0.65 23.8 17.07"/>
				<polygon points="59.36 11.42 46.22 0 46.22 17.07 49.18 17.07 49.18 6.29 62.32 17.72 62.32 0.65 59.36 0.65 59.36 11.42"/>
				<path d="M91.85,7.58c2.6.19,4.09,1.82,4.09,3.72,0,2.3-1.79,3.56-4.12,3.56-2.18,0-3.83-1.12-3.83-3a2.87,2.87,0,0,1,.33-1.32,7.92,7.92,0,0,0,3.84,1l.3-2.22c-2.29,0-4-1.23-4-3.22S90.08,2.9,92.72,2.9a8.64,8.64,0,0,1,4.87,1.34V1.56a10.21,10.21,0,0,0-5-1.15C88.24.41,85.44,2.65,85.44,6a4.88,4.88,0,0,0,1.14,3.17,4.06,4.06,0,0,0-1.45,3.1c0,3.17,2.57,5.13,6.66,5.13s6.95-2.46,6.95-5.6A4.69,4.69,0,0,0,97.06,8h4.2l.36-2.38H91.85Z"/>
				<path d="M140,5.82C140,3,137.74.65,134.41.65h-8.56V17.07h3V11h5.53C137.73,11,140,8.7,140,5.82Zm-6,2.59h-5.14V3.34H134a2.6,2.6,0,0,1,2.88,2.53A2.61,2.61,0,0,1,134,8.41Z"/>
				<path d="M154.08,0l-9.66,17.07h3.34l2-3.49h8.74l2,3.49h3.36Zm-3,11.15,3-5.4,3,5.4Z"/>
				<path d="M186.82,5.82c0-2.87-2.29-5.17-5.62-5.17h-8.56V17.07h3V11h5.53C184.52,11,186.82,8.7,186.82,5.82Zm-6,2.59h-5.14V3.34h5.14a2.6,2.6,0,0,1,2.88,2.53A2.61,2.61,0,0,1,180.81,8.41Z"/>
				<polygon points="208.68 14.39 199.37 14.39 199.37 10.16 208.29 10.16 208.29 7.55 199.37 7.55 199.37 3.39 208.68 3.39 208.68 0.65 196.35 0.65 196.35 17.07 208.68 17.07 208.68 14.39"/>
				<path d="M229.6,17.07h3.61l-4.85-6.53c2.76-.44,4.57-2.37,4.57-4.87,0-2.79-2.25-5-5.57-5h-8.6V17.07h3V10.71H225ZM221.78,3.32H227a2.53,2.53,0,0,1,2.8,2.41,2.53,2.53,0,0,1-2.8,2.4h-5.23Z"/>
				<polygon points="4.02 28.68 0.83 28.68 0.83 26.03 0 26.03 0 32.21 0.83 32.21 0.83 29.45 4.02 29.45 4.02 32.21 4.85 32.21 4.85 26.03 4.02 26.03 4.02 28.68"/>
				<path d="M10.05,26,7.57,32.21h.9l.62-1.58h2.78l.62,1.58h.91L10.92,26Zm-.66,3.83,1.08-2.76,1.09,2.76Z"/>
				<polygon points="20.35 30.74 17.16 26.03 16.12 26.03 16.12 32.21 16.93 32.21 16.93 27.12 20.31 32.21 21.16 32.21 21.16 26.03 20.35 26.03 20.35 30.74"/>
				<path d="M26.73,26H24.58v6.18h2.14a2.84,2.84,0,0,0,3-3.07A2.86,2.86,0,0,0,26.73,26Zm0,5.41H25.41V26.8h1.3a2.08,2.08,0,0,1,2.11,2.34A2.06,2.06,0,0,1,26.69,31.44Z"/>
				<rect x="32.56" y="28.73" width="2.48" height="0.72"/>
				<path d="M40.35,26H38.13v6.18H39v-2.3h1.4a1.94,1.94,0,0,0,0-3.88Zm-.14,3.11H39V26.8h1.26a1.17,1.17,0,1,1,0,2.34Z"/>
				<rect x="45.1" y="26.03" width="0.83" height="6.18"/>
				<path d="M49,29.12a3,3,0,0,0,3.14,3.22,4.21,4.21,0,0,0,2.1-.56V30.9a3.8,3.8,0,0,1-2.1.66,2.24,2.24,0,0,1-2.28-2.44,2.23,2.23,0,0,1,2.28-2.43,3.8,3.8,0,0,1,2.1.66v-.88a4.11,4.11,0,0,0-2.1-.57A3,3,0,0,0,49,29.12Z"/>
				<polygon points="62.18 26.03 61.05 26.03 58.11 29.09 58.11 26.03 57.28 26.03 57.28 32.21 58.11 32.21 58.11 30.2 59.04 29.25 61.18 32.21 62.22 32.21 59.61 28.66 62.18 26.03"/>
				<polygon points="64.96 32.21 68.72 32.21 68.72 31.44 65.79 31.44 65.79 29.49 68.45 29.49 68.45 28.72 65.79 28.72 65.79 26.8 68.72 26.8 68.72 26.03 64.96 26.03 64.96 32.21"/>
				<path d="M74,26H71.8v6.18h2.13a2.84,2.84,0,0,0,3-3.07A2.86,2.86,0,0,0,74,26Zm0,5.41H72.63V26.8h1.29A2.07,2.07,0,0,1,76,29.14,2.06,2.06,0,0,1,73.91,31.44Z"/>
				<polygon points="87.38 29.84 85.05 26.03 84.2 26.03 84.2 32.21 85.01 32.21 85.01 27.48 87.33 31.24 87.41 31.24 89.74 27.48 89.74 32.21 90.56 32.21 90.56 26.03 89.72 26.03 87.38 29.84"/>
				<path d="M95.76,26l-2.48,6.18h.9l.62-1.58h2.78l.62,1.58h.91L96.63,26Zm-.66,3.83,1.07-2.76,1.1,2.76Z"/>
				<path d="M106,27.94a1.9,1.9,0,0,0-2-1.91h-2.22v6.18h.82V29.83h1.22l1.31,2.38h.93l-1.39-2.47A1.77,1.77,0,0,0,106,27.94Zm-3.37,1.12V26.8H104a1.11,1.11,0,0,1,1.18,1.14A1.06,1.06,0,0,1,104,29.06Z"/>
				<polygon points="114.02 26.03 112.89 26.03 109.95 29.09 109.95 26.03 109.13 26.03 109.13 32.21 109.95 32.21 109.95 30.2 110.88 29.25 113.02 32.21 114.06 32.21 111.45 28.66 114.02 26.03"/>
				<polygon points="116.8 32.21 120.57 32.21 120.57 31.44 117.63 31.44 117.63 29.49 120.29 29.49 120.29 28.72 117.63 28.72 117.63 26.8 120.57 26.8 120.57 26.03 116.8 26.03 116.8 32.21"/>
				<polygon points="123.28 26.8 125.33 26.8 125.33 32.21 126.16 32.21 126.16 26.8 128.2 26.8 128.2 26.03 123.28 26.03 123.28 26.8"/>
				<rect x="131.06" y="26.03" width="0.83" height="6.18"/>
				<polygon points="139.52 30.74 136.33 26.03 135.3 26.03 135.3 32.21 136.11 32.21 136.11 27.12 139.49 32.21 140.33 32.21 140.33 26.03 139.52 26.03 139.52 30.74"/>
				<path d="M143.39,29.12a3,3,0,0,0,3.15,3.22,4.17,4.17,0,0,0,2.09-.56V29.15h-2.94v.77h2.13v1.36a3.09,3.09,0,0,1-1.28.27,2.23,2.23,0,0,1-2.28-2.43,2.23,2.23,0,0,1,2.28-2.43,3.77,3.77,0,0,1,2.09.66v-.88a4.08,4.08,0,0,0-2.09-.57A3.05,3.05,0,0,0,143.39,29.12Z"/>
				<polygon points="156.34 32.21 157.16 32.21 157.16 29.58 159.83 29.58 159.83 28.81 157.16 28.81 157.16 26.8 160.1 26.8 160.1 26.03 156.34 26.03 156.34 32.21"/>
				<path d="M167.13,27.94a1.9,1.9,0,0,0-2-1.91h-2.23v6.18h.83V29.83H165l1.31,2.38h.93l-1.39-2.47A1.77,1.77,0,0,0,167.13,27.94Zm-3.37,1.12V26.8h1.33a1.11,1.11,0,0,1,1.18,1.14,1.06,1.06,0,0,1-1.18,1.12Z"/>
				<polygon points="170.22 32.21 173.99 32.21 173.99 31.44 171.05 31.44 171.05 29.49 173.72 29.49 173.72 28.72 171.05 28.72 171.05 26.8 173.99 26.8 173.99 26.03 170.22 26.03 170.22 32.21"/>
				<polygon points="177.07 32.21 180.83 32.21 180.83 31.44 177.9 31.44 177.9 29.49 180.56 29.49 180.56 28.72 177.9 28.72 177.9 26.8 180.83 26.8 180.83 26.03 177.07 26.03 177.07 32.21"/>
				<polygon points="184.74 26.03 183.91 26.03 183.91 32.21 187.69 32.21 187.69 31.41 184.74 31.41 184.74 26.03"/>
				<path d="M192.7,26l-2.48,6.18h.9l.62-1.58h2.78l.62,1.58h.91L193.57,26ZM192,29.86l1.07-2.76,1.1,2.76Z"/>
				<polygon points="203 30.74 199.8 26.03 198.77 26.03 198.77 32.21 199.58 32.21 199.58 27.12 202.96 32.21 203.81 32.21 203.81 26.03 203 26.03 203 30.74"/>
				<path d="M206.87,29.12A3,3,0,0,0,210,32.34a4.2,4.2,0,0,0,2.09-.56V30.9a3.77,3.77,0,0,1-2.09.66,2.24,2.24,0,0,1-2.29-2.44A2.23,2.23,0,0,1,210,26.69a3.77,3.77,0,0,1,2.09.66v-.88A4.1,4.1,0,0,0,210,25.9,3,3,0,0,0,206.87,29.12Z"/>
				<polygon points="215.18 32.21 218.94 32.21 218.94 31.44 216 31.44 216 29.49 218.67 29.49 218.67 28.72 216 28.72 216 26.8 218.94 26.8 218.94 26.03 215.18 26.03 215.18 32.21"/>
				<path d="M226.22,27.94a1.91,1.91,0,0,0-2-1.91H222v6.18h.83V29.83h1.21l1.31,2.38h.93l-1.39-2.47A1.78,1.78,0,0,0,226.22,27.94Zm-3.37,1.12V26.8h1.32a1.11,1.11,0,0,1,1.18,1.14,1.06,1.06,0,0,1-1.18,1.12Z"/>
				<path d="M231.61,28.76l-.94-.2a.89.89,0,0,1-.8-.91c0-.61.52-1,1.33-1a3.92,3.92,0,0,1,1.7.45v-.85a4.21,4.21,0,0,0-1.7-.36c-1.4,0-2.18.8-2.18,1.75a1.65,1.65,0,0,0,1.5,1.7l.89.2a1,1,0,0,1,.94,1c0,.59-.34,1-1.2,1a4.42,4.42,0,0,1-2.08-.6v.83a4.23,4.23,0,0,0,2.08.54,1.86,1.86,0,0,0,2.06-1.81A1.78,1.78,0,0,0,231.61,28.76Z"/>
			</svg>
			<nav class="relative flex items-center 2xl:fixed 2xl:top-0 2xl:right-0 2xl:bottom-0 2xl:flex-col 2xl:justify-center 2xl:items-end 2xl:w-[26rem] 2xl:bg-gradient-to-b 2xl:from-purple 2xl:to-blue 2xl:pt-32 2xl:px-12 2xl:pb-36 2xl:shadow-xl 2xl:transition 2xl:duration-300 2xl:transform 2xl:translate-x-full 2xl:z-40 sm:w-full sm:pt-24 sm:px-6 navMenu">
				<button type="button" class="hidden absolute top-10 right-12 text-white 2xl:block lg:top-4 lg:right-4 navClose" aria-label="Close menu">
					<svg aria-hidden="true" focusable="false" class="w-8 h-8 fill-current">
						<use xlink:href="{{ asset('images/icons.svg#close') }}" />
					</svg>
				</button>
				<ul class="flex justify-center items-center 2xl:flex-col 2xl:items-center 2xl:w-full">
					<li class="pr-16 2xl:pr-0 2xl:pb-8">
						<a href="#features" class="text-sm font-semibold uppercase tracking-widest text-white transition-colors duration-100 hover:text-purple 2xl:text-4xl 2xl:text-white 2xl:tracking-normal 2xl:hover:text-white animateScroll">Features</a>
					</li>
					<li class="pr-16 2xl:pr-0 2xl:pb-8">
						<a href="#case-studies" class="text-sm font-semibold uppercase tracking-widest text-white transition-colors duration-100 hover:text-purple 2xl:text-4xl 2xl:text-white 2xl:tracking-normal 2xl:hover:text-white animateScroll">Case studies</a>
					</li>
					<li class="pr-16 2xl:pr-0 2xl:pb-8">
						<a href="#what-we-do" class="text-sm font-semibold uppercase tracking-widest text-white transition-colors duration-100 hover:text-purple 2xl:text-4xl 2xl:text-white 2xl:tracking-normal 2xl:hover:text-white animateScroll">What we do</a>
					</li>
					<li class="pr-16 2xl:pr-0 2xl:pb-8">
						<a href="#how-it-works" class="text-sm font-semibold uppercase tracking-widest text-white transition-colors duration-100 hover:text-purple 2xl:text-4xl 2xl:text-white 2xl:tracking-normal 2xl:hover:text-white animateScroll">How it works</a>
					</li>
					<li class="hidden 2xl:block">
						<a href="#contact" class="text-sm font-semibold uppercase tracking-widest text-white transition-colors duration-100 hover:text-purple 2xl:text-4xl 2xl:text-white 2xl:tracking-normal 2xl:hover:text-white animateScroll">Get in touch</a>
					</li>
				</ul>
				<a href="#contact" class="inline-flex justify-center items-center w-64 h-12 text-white bg-btn-blue bg-cover bg-center bg-no-repeat transition-colors duration-100 hover:bg-btn-blue-hover 2xl:hidden navBtn animateScroll">
					<span class="font-semibold uppercase tracking-widest pr-8">Get in touch</span>
					<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
						<use href="{{ asset('images/icons.svg#arrow-right') }}" />
					</svg>
				</a>
				<div class="absolute left-0 bottom-16 justify-center items-center w-full hidden 2xl:flex">
					<a href="https://www.linkedin.com/company/pen-paper-talent" class="flex justify-center items-center w-10 h-10 text-white mr-6 rounded-full border border-white transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple" target="_blank" rel="noopener">
						<svg aria-hidden="true" focusable="false" class="w-4 h-4 fill-current">
							<use href="{{ asset('images/icons.svg#linkedin') }}" />
						</svg>
					</a>
					<a href="https://www.facebook.com/penpapermarketing" class="flex justify-center items-center w-10 h-10 text-white rounded-full border border-white transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple" target="_blank" rel="noopener">
						<svg aria-hidden="true" focusable="false" class="w-4 h-4 fill-current">
							<use href="{{ asset('images/icons.svg#facebook') }}" />
						</svg>
					</a>
					{{-- <a href="https://behance.com" class="flex justify-center items-center w-10 h-10 text-white rounded-full border border-white transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple" target="_blank" rel="noopener">
						<svg aria-hidden="true" focusable="false" class="w-4 h-4 fill-current">
							<use href="{{ asset('images/icons.svg#behance') }}" />
						</svg>
					</a> --}}
				</div>
			</nav>
			<button type="button" class="hidden text-white 2xl:block navOpen" aria-label="Menu">
				<svg aria-hidden="true" focusable="false" class="w-8 h-8 fill-current">
					<use xlink:href="{{ asset('images/icons.svg#menu') }}" />
				</svg>
			</button>
		</div>
	</header>

    @yield('content')

	<footer class="bg-gray-300 bg-cover bg-bottom bg-no-repeat" style="background-image: url({{ asset('images/pen-paper-1.jpg') }})">
		<div class="max-w-7xl w-full px-4 m-auto">
			<section class="flex flex-col items-center pt-36 pb-24 border-b border-white border-opacity-20 lg:pt-24 sm:pt-12 sm:pb-12">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 233.21 32.34" class="w-80 h-auto fill-current text-white mb-24 lg:mb-0 sm:w-64">
					<path d="M8.66.65H.1V17.07h3V11H8.66C12,11,14.27,8.7,14.27,5.82S12,.65,8.66.65ZM8.27,8.41H3.13V3.34H8.27a2.61,2.61,0,0,1,2.88,2.53A2.61,2.61,0,0,1,8.27,8.41Z"/>
					<polygon points="23.8 17.07 36.14 17.07 36.14 14.39 26.83 14.39 26.83 10.17 35.75 10.17 35.75 7.55 26.83 7.55 26.83 3.39 36.14 3.39 36.14 0.65 23.8 0.65 23.8 17.07"/>
					<polygon points="59.36 11.42 46.22 0 46.22 17.07 49.18 17.07 49.18 6.29 62.32 17.72 62.32 0.65 59.36 0.65 59.36 11.42"/>
					<path d="M91.85,7.58c2.6.19,4.09,1.82,4.09,3.72,0,2.3-1.79,3.56-4.12,3.56-2.18,0-3.83-1.12-3.83-3a2.87,2.87,0,0,1,.33-1.32,7.92,7.92,0,0,0,3.84,1l.3-2.22c-2.29,0-4-1.23-4-3.22S90.08,2.9,92.72,2.9a8.64,8.64,0,0,1,4.87,1.34V1.56a10.21,10.21,0,0,0-5-1.15C88.24.41,85.44,2.65,85.44,6a4.88,4.88,0,0,0,1.14,3.17,4.06,4.06,0,0,0-1.45,3.1c0,3.17,2.57,5.13,6.66,5.13s6.95-2.46,6.95-5.6A4.69,4.69,0,0,0,97.06,8h4.2l.36-2.38H91.85Z"/>
					<path d="M140,5.82C140,3,137.74.65,134.41.65h-8.56V17.07h3V11h5.53C137.73,11,140,8.7,140,5.82Zm-6,2.59h-5.14V3.34H134a2.6,2.6,0,0,1,2.88,2.53A2.61,2.61,0,0,1,134,8.41Z"/>
					<path d="M154.08,0l-9.66,17.07h3.34l2-3.49h8.74l2,3.49h3.36Zm-3,11.15,3-5.4,3,5.4Z"/>
					<path d="M186.82,5.82c0-2.87-2.29-5.17-5.62-5.17h-8.56V17.07h3V11h5.53C184.52,11,186.82,8.7,186.82,5.82Zm-6,2.59h-5.14V3.34h5.14a2.6,2.6,0,0,1,2.88,2.53A2.61,2.61,0,0,1,180.81,8.41Z"/>
					<polygon points="208.68 14.39 199.37 14.39 199.37 10.16 208.29 10.16 208.29 7.55 199.37 7.55 199.37 3.39 208.68 3.39 208.68 0.65 196.35 0.65 196.35 17.07 208.68 17.07 208.68 14.39"/>
					<path d="M229.6,17.07h3.61l-4.85-6.53c2.76-.44,4.57-2.37,4.57-4.87,0-2.79-2.25-5-5.57-5h-8.6V17.07h3V10.71H225ZM221.78,3.32H227a2.53,2.53,0,0,1,2.8,2.41,2.53,2.53,0,0,1-2.8,2.4h-5.23Z"/>
					<polygon points="4.02 28.68 0.83 28.68 0.83 26.03 0 26.03 0 32.21 0.83 32.21 0.83 29.45 4.02 29.45 4.02 32.21 4.85 32.21 4.85 26.03 4.02 26.03 4.02 28.68"/>
					<path d="M10.05,26,7.57,32.21h.9l.62-1.58h2.78l.62,1.58h.91L10.92,26Zm-.66,3.83,1.08-2.76,1.09,2.76Z"/>
					<polygon points="20.35 30.74 17.16 26.03 16.12 26.03 16.12 32.21 16.93 32.21 16.93 27.12 20.31 32.21 21.16 32.21 21.16 26.03 20.35 26.03 20.35 30.74"/>
					<path d="M26.73,26H24.58v6.18h2.14a2.84,2.84,0,0,0,3-3.07A2.86,2.86,0,0,0,26.73,26Zm0,5.41H25.41V26.8h1.3a2.08,2.08,0,0,1,2.11,2.34A2.06,2.06,0,0,1,26.69,31.44Z"/>
					<rect x="32.56" y="28.73" width="2.48" height="0.72"/>
					<path d="M40.35,26H38.13v6.18H39v-2.3h1.4a1.94,1.94,0,0,0,0-3.88Zm-.14,3.11H39V26.8h1.26a1.17,1.17,0,1,1,0,2.34Z"/>
					<rect x="45.1" y="26.03" width="0.83" height="6.18"/>
					<path d="M49,29.12a3,3,0,0,0,3.14,3.22,4.21,4.21,0,0,0,2.1-.56V30.9a3.8,3.8,0,0,1-2.1.66,2.24,2.24,0,0,1-2.28-2.44,2.23,2.23,0,0,1,2.28-2.43,3.8,3.8,0,0,1,2.1.66v-.88a4.11,4.11,0,0,0-2.1-.57A3,3,0,0,0,49,29.12Z"/>
					<polygon points="62.18 26.03 61.05 26.03 58.11 29.09 58.11 26.03 57.28 26.03 57.28 32.21 58.11 32.21 58.11 30.2 59.04 29.25 61.18 32.21 62.22 32.21 59.61 28.66 62.18 26.03"/>
					<polygon points="64.96 32.21 68.72 32.21 68.72 31.44 65.79 31.44 65.79 29.49 68.45 29.49 68.45 28.72 65.79 28.72 65.79 26.8 68.72 26.8 68.72 26.03 64.96 26.03 64.96 32.21"/>
					<path d="M74,26H71.8v6.18h2.13a2.84,2.84,0,0,0,3-3.07A2.86,2.86,0,0,0,74,26Zm0,5.41H72.63V26.8h1.29A2.07,2.07,0,0,1,76,29.14,2.06,2.06,0,0,1,73.91,31.44Z"/>
					<polygon points="87.38 29.84 85.05 26.03 84.2 26.03 84.2 32.21 85.01 32.21 85.01 27.48 87.33 31.24 87.41 31.24 89.74 27.48 89.74 32.21 90.56 32.21 90.56 26.03 89.72 26.03 87.38 29.84"/>
					<path d="M95.76,26l-2.48,6.18h.9l.62-1.58h2.78l.62,1.58h.91L96.63,26Zm-.66,3.83,1.07-2.76,1.1,2.76Z"/>
					<path d="M106,27.94a1.9,1.9,0,0,0-2-1.91h-2.22v6.18h.82V29.83h1.22l1.31,2.38h.93l-1.39-2.47A1.77,1.77,0,0,0,106,27.94Zm-3.37,1.12V26.8H104a1.11,1.11,0,0,1,1.18,1.14A1.06,1.06,0,0,1,104,29.06Z"/>
					<polygon points="114.02 26.03 112.89 26.03 109.95 29.09 109.95 26.03 109.13 26.03 109.13 32.21 109.95 32.21 109.95 30.2 110.88 29.25 113.02 32.21 114.06 32.21 111.45 28.66 114.02 26.03"/>
					<polygon points="116.8 32.21 120.57 32.21 120.57 31.44 117.63 31.44 117.63 29.49 120.29 29.49 120.29 28.72 117.63 28.72 117.63 26.8 120.57 26.8 120.57 26.03 116.8 26.03 116.8 32.21"/>
					<polygon points="123.28 26.8 125.33 26.8 125.33 32.21 126.16 32.21 126.16 26.8 128.2 26.8 128.2 26.03 123.28 26.03 123.28 26.8"/>
					<rect x="131.06" y="26.03" width="0.83" height="6.18"/>
					<polygon points="139.52 30.74 136.33 26.03 135.3 26.03 135.3 32.21 136.11 32.21 136.11 27.12 139.49 32.21 140.33 32.21 140.33 26.03 139.52 26.03 139.52 30.74"/>
					<path d="M143.39,29.12a3,3,0,0,0,3.15,3.22,4.17,4.17,0,0,0,2.09-.56V29.15h-2.94v.77h2.13v1.36a3.09,3.09,0,0,1-1.28.27,2.23,2.23,0,0,1-2.28-2.43,2.23,2.23,0,0,1,2.28-2.43,3.77,3.77,0,0,1,2.09.66v-.88a4.08,4.08,0,0,0-2.09-.57A3.05,3.05,0,0,0,143.39,29.12Z"/>
					<polygon points="156.34 32.21 157.16 32.21 157.16 29.58 159.83 29.58 159.83 28.81 157.16 28.81 157.16 26.8 160.1 26.8 160.1 26.03 156.34 26.03 156.34 32.21"/>
					<path d="M167.13,27.94a1.9,1.9,0,0,0-2-1.91h-2.23v6.18h.83V29.83H165l1.31,2.38h.93l-1.39-2.47A1.77,1.77,0,0,0,167.13,27.94Zm-3.37,1.12V26.8h1.33a1.11,1.11,0,0,1,1.18,1.14,1.06,1.06,0,0,1-1.18,1.12Z"/>
					<polygon points="170.22 32.21 173.99 32.21 173.99 31.44 171.05 31.44 171.05 29.49 173.72 29.49 173.72 28.72 171.05 28.72 171.05 26.8 173.99 26.8 173.99 26.03 170.22 26.03 170.22 32.21"/>
					<polygon points="177.07 32.21 180.83 32.21 180.83 31.44 177.9 31.44 177.9 29.49 180.56 29.49 180.56 28.72 177.9 28.72 177.9 26.8 180.83 26.8 180.83 26.03 177.07 26.03 177.07 32.21"/>
					<polygon points="184.74 26.03 183.91 26.03 183.91 32.21 187.69 32.21 187.69 31.41 184.74 31.41 184.74 26.03"/>
					<path d="M192.7,26l-2.48,6.18h.9l.62-1.58h2.78l.62,1.58h.91L193.57,26ZM192,29.86l1.07-2.76,1.1,2.76Z"/>
					<polygon points="203 30.74 199.8 26.03 198.77 26.03 198.77 32.21 199.58 32.21 199.58 27.12 202.96 32.21 203.81 32.21 203.81 26.03 203 26.03 203 30.74"/>
					<path d="M206.87,29.12A3,3,0,0,0,210,32.34a4.2,4.2,0,0,0,2.09-.56V30.9a3.77,3.77,0,0,1-2.09.66,2.24,2.24,0,0,1-2.29-2.44A2.23,2.23,0,0,1,210,26.69a3.77,3.77,0,0,1,2.09.66v-.88A4.1,4.1,0,0,0,210,25.9,3,3,0,0,0,206.87,29.12Z"/>
					<polygon points="215.18 32.21 218.94 32.21 218.94 31.44 216 31.44 216 29.49 218.67 29.49 218.67 28.72 216 28.72 216 26.8 218.94 26.8 218.94 26.03 215.18 26.03 215.18 32.21"/>
					<path d="M226.22,27.94a1.91,1.91,0,0,0-2-1.91H222v6.18h.83V29.83h1.21l1.31,2.38h.93l-1.39-2.47A1.78,1.78,0,0,0,226.22,27.94Zm-3.37,1.12V26.8h1.32a1.11,1.11,0,0,1,1.18,1.14,1.06,1.06,0,0,1-1.18,1.12Z"/>
					<path d="M231.61,28.76l-.94-.2a.89.89,0,0,1-.8-.91c0-.61.52-1,1.33-1a3.92,3.92,0,0,1,1.7.45v-.85a4.21,4.21,0,0,0-1.7-.36c-1.4,0-2.18.8-2.18,1.75a1.65,1.65,0,0,0,1.5,1.7l.89.2a1,1,0,0,1,.94,1c0,.59-.34,1-1.2,1a4.42,4.42,0,0,1-2.08-.6v.83a4.23,4.23,0,0,0,2.08.54,1.86,1.86,0,0,0,2.06-1.81A1.78,1.78,0,0,0,231.61,28.76Z"/>
				</svg>
				<ul class="flex justify-center items-center lg:hidden">
					<li class="pr-20">
						<a href="#features" class="text-sm font-bold uppercase tracking-widest text-white transition-colors duration-100 hover:text-purple animateScroll">Features</a>
					</li>
					<li class="pr-20">
						<a href="#case-studies" class="text-sm font-bold uppercase tracking-widest text-white transition-colors duration-100 hover:text-purple animateScroll">Case studies</a>
					</li>
					<li class="pr-20">
						<a href="#what-we-do" class="text-sm font-bold uppercase tracking-widest text-white transition-colors duration-100 hover:text-purple animateScroll">What we do</a>
					</li>
					<li>
						<a href="#how-it-works" class="text-sm font-bold uppercase tracking-widest text-white transition-colors duration-100 hover:text-purple animateScroll">How it works</a>
					</li>
				</ul>
			</section>
			<section class="py-20 border-b border-white border-opacity-20 sm:py-12">
				<div class="flex flex-wrap justify-center items-center -m-8 sm:-m-6">
					<span class="p-8 sm:p-6">
						<img src="{{ asset('images/logos/tesco.png') }}" alt="Tesco" class="max-w-[8rem] max-h-[3rem] sm:max-w-[6rem] sm:max-h-[2rem]">
					</span>
					<span class="p-8 sm:p-6">
						<img src="{{ asset('images/logos/slovak-telekom.png') }}" alt="Slovak Telekom" class="max-w-[8rem] max-h-[3rem] sm:max-w-[6rem] sm:max-h-[2rem]">
					</span>
					<span class="p-8 sm:p-6">
						<img src="{{ asset('images/logos/solved.png') }}" alt="Solved" class="max-w-[8rem] max-h-[3rem] sm:max-w-[6rem] sm:max-h-[2rem]">
					</span>
					<span class="p-8 sm:p-6">
						<img src="{{ asset('images/logos/promobay.png') }}" alt="Promobay" class="max-w-[8rem] max-h-[3rem] sm:max-w-[6rem] sm:max-h-[2rem]">
					</span>
					<span class="p-8 sm:p-6">
						<img src="{{ asset('images/logos/joico.png') }}" alt="Joico" class="max-w-[8rem] max-h-[3rem] sm:max-w-[6rem] sm:max-h-[2rem]">
					</span>
					<span class="p-8 sm:p-6">
						<img src="{{ asset('images/logos/decent.png') }}" alt="Decent" class="max-w-[8rem] max-h-[3rem] sm:max-w-[6rem] sm:max-h-[2rem]">
					</span>
					<span class="p-8 sm:p-6">
						<img src="{{ asset('images/logos/digi.png') }}" alt="Digi" class="max-w-[8rem] max-h-[3rem] sm:max-w-[6rem] sm:max-h-[2rem]">
					</span>
				</div>
			</section>
			<section class="py-12">
				<div class="flex justify-between items-center sm:flex-col sm:justify-start">
					<span class="text-sm text-white sm:order-2">Copyright 2021</span>
					<span class="flex items-center sm:flex-wrap sm:justify-center sm:order-1 sm:pb-4">
						<a href="https://www.linkedin.com/company/pen-paper-talent" class="flex justify-center items-center w-10 h-10 text-white mr-6 rounded-full border border-white transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple" target="_blank" rel="noopener">
							<svg aria-hidden="true" focusable="false" class="w-4 h-4 fill-current">
								<use href="{{ asset('images/icons.svg#linkedin') }}" />
							</svg>
						</a>
						<a href="https://www.facebook.com/penpapermarketing" class="flex justify-center items-center w-10 h-10 text-white rounded-full border border-white transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple" target="_blank" rel="noopener">
							<svg aria-hidden="true" focusable="false" class="w-4 h-4 fill-current">
								<use href="{{ asset('images/icons.svg#facebook') }}" />
							</svg>
						</a>
						{{-- <a href="https://behance.com" class="flex justify-center items-center w-10 h-10 text-white rounded-full border border-white transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple" target="_blank" rel="noopener">
							<svg aria-hidden="true" focusable="false" class="w-4 h-4 fill-current">
								<use href="{{ asset('images/icons.svg#behance') }}" />
							</svg>
						</a> --}}
					</span>
				</div>
			</section>
		</div>
	</footer>

	<script>var exports = {};</script>
    <script src="{{ asset(mix('js/vendor.js')) }}" defer></script>
    <script src="{{ asset(mix('js/app.js')) }}" defer></script>

	<script src='https://www.google.com/recaptcha/api.js?render={{ env('GOOGLE_RECAPTCHA_KEY') }}'></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('{{ env('GOOGLE_RECAPTCHA_KEY') }}', { action: 'form' }).then(function (token) {
                var recaptchaResponse = document.getElementsByClassName('g-recaptcha-response');
                for (i = 0; i < recaptchaResponse.length; i++) {
                    recaptchaResponse[i].value = token;
                }
            });
        });
    </script>
</body>
</html>
