@extends('layouts.app')

@section('content')
	<main>
		<section class="relative pt-36 sm:pt-24 xs:pt-16">
			<div class="absolute top-0 right-0 bottom-0 w-screen bg-hero-wide bg-right-bottom bg-cover bg-no-repeat z-[-1] 3xl:bg-hero-desktop lg:bg-hero-mobile lg:bg-bottom lg:after:absolute lg:after:top-0 lg:after:left-0 lg:after:right-0 lg:after:bottom-0 lg:after:bg-black lg:after:bg-opacity-60"></div>
			<div class="pt-36 pb-48 lg:pb-36 sm:pt-24 sm:pb-24">
				<div class="max-w-7xl w-full px-12 m-auto lg:px-4">
					<div class="max-w-xl w-full lg:max-w-none lg:text-center">
						<h1 class="text-9xl font-semibold uppercase text-white pb-12 md:text-8xl sm:text-7xl xs:text-6xl">Creators unite!</h1>
						<p class="text-3xl font-light text-white pb-12 sm:text-2xl">The remote talent pool of proven, senior CEE advertising specialists.</p>
						<a href="#why-should-you-care" class="inline-flex justify-between items-center text-white bg-btn-blue bg-contain bg-center bg-no-repeat pl-8 py-5 pr-6 hover:bg-btn-blue-hover animateScroll">
							<span class="font-semibold uppercase tracking-widest pr-8">Why Should I Care?</span>
							<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
								<use href="{{ asset('images/icons.svg#arrow-right') }}" />
							</svg>
						</a>
					</div>
				</div>
			</div>
			<div class="py-14 3xl:py-[5.5rem] sm:py-12">
				<div class="max-w-7xl w-full px-4 m-auto xl:px-0">
					<div class="flex justify-center items-center -m-8 xl:m-0 logoSliderSlides">
						<span class="text-center p-8">
							<img src="{{ asset('images/logos/tesco.png') }}" alt="Tesco" class="inline-block max-w-[8rem] max-h-[3rem] sm:max-w-[6rem]">
						</span>
						<span class="text-center p-8">
							<img src="{{ asset('images/logos/slovak-telekom.png') }}" alt="Slovak Telekom" class="inline-block max-w-[8rem] max-h-[3rem] sm:max-w-[6rem]">
						</span>
						<span class="text-center p-8">
							<img src="{{ asset('images/logos/solved.png') }}" alt="Solved" class="inline-block max-w-[8rem] max-h-[3rem] sm:max-w-[6rem]">
						</span>
						<span class="text-center p-8">
							<img src="{{ asset('images/logos/promobay.png') }}" alt="Promobay" class="inline-block max-w-[8rem] max-h-[3rem] sm:max-w-[6rem]">
						</span>
						<span class="text-center p-8">
							<img src="{{ asset('images/logos/joico.png') }}" alt="Joico" class="inline-block max-w-[8rem] max-h-[3rem] sm:max-w-[6rem]">
						</span>
						<span class="text-center p-8">
							<img src="{{ asset('images/logos/decent.png') }}" alt="Decent" class="inline-block max-w-[8rem] max-h-[3rem] sm:max-w-[6rem]">
						</span>
						<span class="text-center p-8">
							<img src="{{ asset('images/logos/digi.png') }}" alt="Digi" class="inline-block max-w-[8rem] max-h-[3rem] sm:max-w-[6rem]">
						</span>
					</div>
				</div>
			</div>
		</section>
		<section id="features" class="relative bg-gray-100 pt-80 pb-36 lg:pb-0 sm:pt-24">
			<div class="relative pb-24 z-20">
				<div class="max-w-6xl w-full px-12 m-auto lg:px-4">
					<div class="relative">
						<img src="{{ asset('images/pen-paper-11.png') }}" class="absolute -top-full left-1/2 max-w-none lg:transform lg:-translate-x-1/2 z-[-1] sm:-top-full sm:max-w-[36rem] xs:-top-2/3">
						<h2 class="text-8xl font-semibold uppercase leading-none text-center text-black pb-36 md:text-7xl md:leading-tight sm:text-6xl sm:leading-none sm:pb-24 xs:text-5xl xs:pb-16">Hand-picked <br>talent for&nbsp;hire</h2>
					</div>
					<div class="flex flex-wrap justify-between">
						<div class="max-w-[28rem] w-full lg:max-w-none lg:order-1">
							<div class="relative">
								<img src="{{ asset('images/pen-paper-10.png') }}" class="absolute top-1/2 left-1/2 max-w-none transform -translate-x-1/2 -translate-y-1/2 z-[-1] lg:hidden">
								<div class="text-xl font-medium leading-loose uppercase text-black lg:text-center">Pen & Paper is a crowd of senior advertising talent for external hire. Hand-picked and tested on local projects in Central and Eastern Europe by experienced agency veterans, our freelancers are guaranteed to be reliable, flexible, and easy to onboard.</div>
							</div>
						</div>
						<div class="max-w-[28rem] w-full lg:max-w-none lg:order-2">
							<img src="{{ asset('images/pen-paper-4.png') }}" class="inline-block pb-12 mt-24">
						</div>
						<div class="max-w-[28rem] w-full lg:max-w-none lg:order-4">
							<img src="{{ asset('images/pen-paper-5.png') }}" class="inline-block pb-12 -mt-24 lg:mt-0">
						</div>
						<div class="max-w-[28rem] w-full lg:max-w-none lg:order-3 lg:pb-24">
							<div class="relative">
								<h3 class="text-5xl font-bold leading-none uppercase text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pb-6 sm:text-4xl">Soft Skills <br>First</h3>
								<div class="text-lg leading-loose text-black">We firmly believe that <strong>character matters</strong>, so we only hand-pick experts with outstanding communication skills and conscientious work ethic.</div>
								<img src="{{ asset('images/pen-paper-12.png') }}" class="absolute right-0 -bottom-2/3 z-[-1] hidden lg:block">
							</div>
						</div>
						<div class="max-w-[28rem] w-full lg:max-w-none lg:order-5 lg:pb-24">
							<div class="relative">
								<img src="{{ asset('images/pen-paper-12.png') }}" class="absolute top-full -left-1/2 z-[-1] lg:hidden">
								<h3 class="text-5xl font-bold leading-none uppercase text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pb-6 sm:text-4xl">Proven <br>Experts Only</h3>
								<div class="text-lg leading-loose text-black">All of the people we offer are <strong>tried and tested thoroughly on local projects first</strong>. We only pick the best; and know many of them personally.</div>
							</div>
						</div>
						<div class="max-w-[28rem] w-full lg:max-w-none lg:order-6">
							<div class="relative">
								<img src="{{ asset('images/pen-paper-6.png') }}" class="inline-block pb-12">
								<img src="{{ asset('images/pen-paper-12.png') }}" class="absolute -top-1/4 -right-1/4 max-w-none z-[-1]">
							</div>
						</div>
						<div class="max-w-[28rem] w-full lg:max-w-none lg:hidden"></div>
						<div class="max-w-[28rem] w-full lg:max-w-none lg:order-7">
							<h3 class="text-5xl font-bold leading-none uppercase text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pb-6 sm:text-4xl">Competitive Prices</h3>
							<div class="text-lg leading-loose text-black"><strong>Based in Central and Eastern Europe</strong>, we can offer high-quality professionals for less due to the optimized living expenses of our experts.</div>
						</div>
					</div>
				</div>
			</div>
			<div class="relative flex justify-center items-center py-32 sm:py-24">
				<img src="{{ asset('images/pen-paper-13.png') }}" class="absolute -top-1/2 left-[20%] z-10 xl:-top-1/2 lg:hidden">
				<div class="absolute top-0 left-0 right-0 bottom-0 bg-try bg-cover bg-bottom bg-no-repeat ml-12 rounded-l-full mr-0 lg:ml-0 lg:rounded-none"></div>
				<div class="relative max-w-[40rem] w-full text-center px-4 z-10">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 331 26" class="inline-block w-40 h-auto fill-current text-black mb-6 sm:text-white">
						<path d="M11.52,12.35A3.75,3.75,0,0,0,15.6,8.63c0-2.12-1.63-3.69-4.08-3.69H4.27v7.41ZM12.08,1A7.53,7.53,0,0,1,20,8.56c0,4.21-3.24,7.53-7.92,7.53H4.27V25H0V1Z"/>
						<polygon points="51.44 11.09 51.44 14.91 38.41 14.91 38.41 21.09 52 21.09 52 25 34 25 34 1 52 1 52 5 38.41 5 38.41 11.09 51.44 11.09"/>
						<polygon points="82.96 17.12 82.96 1.92 87 1.92 87 26 69.05 9.88 69.05 25.09 65 25.09 65 1 82.96 17.12"/>
						<path d="M143.47,11.79h-6.11a6.56,6.56,0,0,1,2.44,5.28c0,4.44-4,7.93-10.1,7.93-5.95,0-9.7-2.78-9.7-7.26a5.72,5.72,0,0,1,2.12-4.4,6.73,6.73,0,0,1-1.67-4.49c0-4.68,4.07-7.85,10.47-7.85a15,15,0,0,1,7.21,1.63v3.8A12.79,12.79,0,0,0,131,4.53c-3.83,0-6.27,1.75-6.27,4.48s2.56,4.57,5.91,4.57l-.45,3.13a11.83,11.83,0,0,1-5.58-1.35,3.93,3.93,0,0,0-.49,1.86c0,2.66,2.4,4.25,5.58,4.25,3.38,0,6-1.79,6-5,0-2.7-2.16-5-5.95-5.27V8.42H144Z"/>
						<path d="M190.52,12.35a3.74,3.74,0,0,0,4.08-3.72c0-2.12-1.63-3.69-4.08-3.69h-7.25v7.41ZM191.08,1A7.53,7.53,0,0,1,199,8.56c0,4.21-3.24,7.53-7.92,7.53h-7.81V25H179V1Z"/>
						<path d="M213.62,16.33h8.71L218,8.42ZM208.84,25H204L218,0l14,25h-4.86l-2.82-5.11H211.66Z"/>
						<path d="M256.52,12.35a3.75,3.75,0,0,0,4.08-3.72c0-2.12-1.63-3.69-4.08-3.69h-7.25v7.41ZM257.08,1A7.53,7.53,0,0,1,265,8.56c0,4.21-3.24,7.53-7.92,7.53h-7.81V25H245V1Z"/>
						<polygon points="295.44 11.09 295.44 14.91 282.41 14.91 282.41 21.09 296 21.09 296 25 278 25 278 1 296 1 296 5 282.41 5 282.41 11.09 295.44 11.09"/>
						<path d="M322.42,11.94a3.58,3.58,0,0,0,3.87-3.52,3.58,3.58,0,0,0-3.87-3.52h-7.24v7ZM315.18,25H311V1h11.9c4.59,0,7.71,3.27,7.71,7.34a7.12,7.12,0,0,1-6.32,7.11L331,25h-5l-6.39-9.3h-4.43Z"/>
					</svg>
					<h2 class="text-5xl font-bold leading-tight uppercase text-black pb-6 sm:text-white">Try before you buy</h2>
					<p class="text-xl font-light text-center text-black pb-12 sm:text-lg sm:pb-6">We like our talents to work closely and regularly with a limited number of partners. To build trust, we like to start with a smaller test job – why not give it a shot before you commit?</p>
					<a href="#contact" class="inline-flex justify-between items-center w-64 text-black pl-8 py-3 pr-6 border-2 border-black rounded-full transition-colors hover:text-white hover:bg-purple hover:border-purple animateScroll">
						<span class="font-semibold uppercase tracking-widest">Brief now</span>
						<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
							<use href="{{ asset('images/icons.svg#arrow-right') }}" />
						</svg>
					</a>
				</div>
			</div>
		</section>
		<section class="bg-gray-200 py-56 sm:py-24">
			<div class="flex flex-col items-center max-w-4xl w-full px-4 m-auto">
				<h2 class="text-8xl font-semibold uppercase leading-tight text-transparent text-center bg-clip-text bg-gradient-to-b from-purple to-pink pb-6 md:text-7xl sm:text-6xl xs:text-5xl">How we roll</h2>
				<p class="text-2xl font-extralight text-center text-white pb-12 sm:text-xl">This is a <strong>selection of what we’re really proud of</strong>. We hold all jobs, brands, awards, and partners very dear since they make us who we are.</p>
				<a href="#case-studies" class="inline-flex justify-center items-center w-14 h-14 text-white border border-white rounded-full transition-all duration-100 opacity-40 hover:text-white hover:bg-purple hover:border-purple hover:opacity-100 animateScroll">
					<svg aria-hidden="true" focusable="false" class="w-4 h-6 fill-current">
						<use href="{{ asset('images/icons.svg#arrow-down') }}" />
					</svg>
				</a>
			</div>
		</section>
		<section id="case-studies" class="relative">
			<div class="absolute top-36 left-0 w-full z-10 lg:hidden caseSliderBtns">
				<div class="max-w-7xl w-full px-12 m-auto lg:px-4">
					<div class="flex justify-end">
						<button type="button" class="flex justify-center items-center w-14 h-14 text-black mr-4 border border-black rounded-full transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple caseSliderPrev">
							<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
								<use href="{{ asset('images/icons.svg#arrow-left') }}" />
							</svg>
						</button>
						<button type="button" class="flex justify-center items-center w-14 h-14 text-black border border-black rounded-full transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple caseSliderNext">
							<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
								<use href="{{ asset('images/icons.svg#arrow-right') }}" />
							</svg>
						</button>
					</div>
				</div>
			</div>
			<div class="relative">
				<button type="button" class="absolute top-1/2 left-4 text-black transition-colors duration-100 transform -translate-y-1/2 hidden z-10 hover:text-purple lg:block caseSliderPrev">
					<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
						<use href="{{ asset('images/icons.svg#arrow-left') }}" />
					</svg>
				</button>
				<button type="button" class="absolute top-1/2 right-4 text-black transition-colors duration-100 transform -translate-y-1/2 hidden z-10 hover:text-purple lg:block caseSliderNext">
					<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
						<use href="{{ asset('images/icons.svg#arrow-right') }}" />
					</svg>
				</button>
				<div class="caseSliderSlides">
					<div class="relative bg-yellow lg:py-24 caseSlide">
						<div class="flex items-center w-full h-[56rem] lg:h-auto lg:text-center">
							<div class="relative max-w-7xl w-full px-4 m-auto">
								<div class="relative flex items-center lg:justify-center z-10">
									<div class="w-96 lg:w-full lg:pb-24 sm:pb-12">
										<h3 class="text-sm font-medium uppercase tracking-widest text-black pb-4 sm:hidden">Network agency</h3>
										<h2 class="text-6xl font-bold leading-none uppercase text-black pb-6 sm:text-5xl">A pitch in <br>a week</h2>
										<h3 class="text-sm font-medium uppercase tracking-widest text-black pb-2 sm:hidden">Challenge</h3>
										<p class="text-2xl font-medium leading-tight uppercase text-black pb-12 lg:pb-0 sm:text-xl">Win a war for a new account at burst capacity</p>
										<a href="#case-study-1" class="inline-flex justify-center w-80 text-xl font-semibold uppercase tracking-widest text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors hover:text-white hover:bg-purple lg:hidden caseBtn">Click, click, boom!</a>
									</div>
								</div>
								<img src="{{ asset('images/pen-paper-20.png') }}" class="absolute top-1/2 right-40 w-auto h-[40rem] transform -translate-y-1/2 lg:hidden">
								<img src="{{ asset('images/pen-paper-20-m.png') }}" class="hidden lg:block lg:m-auto lg:h-[24rem] lg:mb-24 sm:mb-12 xs:h-[20rem]">
								<a href="#case-study-1" class="justify-center w-80 text-xl font-semibold uppercase tracking-widest text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors hidden hover:text-white hover:bg-purple lg:inline-flex sm:text-lg caseBtn">Click, click, boom!</a>
							</div>
						</div>
					</div>
					<div class="relative bg-green lg:py-24 caseSlide">
						<div class="flex items-center w-full h-[56rem] lg:h-auto lg:text-center">
							<div class="relative max-w-7xl w-full px-4 m-auto">
								<div class="relative flex items-center lg:justify-center z-10">
									<div class="w-96 lg:w-full lg:pb-24 sm:pb-12">
										<h3 class="text-sm font-medium uppercase tracking-widest text-black pb-4 sm:hidden">Growing indie agency</h3>
										<h2 class="text-6xl font-bold leading-none uppercase text-black pb-6 sm:text-5xl">Test period with a megabrand</h2>
										<h3 class="text-sm font-medium uppercase tracking-widest text-black pb-2 sm:hidden">Challenge</h3>
										<p class="text-2xl font-medium leading-tight uppercase text-black pb-12 lg:pb-0 sm:text-xl">Help a team expand rapidly</p>
										<a href="#case-study-2" class="inline-flex justify-center w-64 text-xl font-semibold uppercase tracking-widest text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors hover:text-white hover:bg-purple lg:hidden caseBtn">Look closer</a>
									</div>
								</div>
								<img src="{{ asset('images/pen-paper-21.png') }}" class="absolute top-1/2 right-40 w-auto h-[40rem] transform -translate-y-1/2 lg:hidden">
								<img src="{{ asset('images/pen-paper-21-m.png') }}" class="hidden lg:block lg:m-auto lg:h-[24rem] lg:mb-24 sm:mb-12 xs:h-[20rem]">
								<a href="#case-study-2" class="justify-center w-64 text-xl font-semibold uppercase tracking-widest text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors hidden hover:text-white hover:bg-purple lg:inline-flex sm:text-lg caseBtn">Look closer</a>
							</div>
						</div>
					</div>
					<div class="relative bg-yellow lg:py-24 caseSlide">
						<div class="flex items-center w-full h-[56rem] lg:h-auto lg:text-center">
							<div class="relative max-w-7xl w-full px-4 m-auto">
								<div class="relative flex items-center lg:justify-center z-10">
									<div class="w-96 lg:w-full lg:pb-24 sm:pb-12">
										<h3 class="text-sm font-medium uppercase tracking-widest text-black pb-4 sm:hidden">ATL Advertising agency</h3>
										<h2 class="text-6xl font-bold leading-none uppercase text-black pb-6 sm:text-5xl">Maximized gains</h2>
										<h3 class="text-sm font-medium uppercase tracking-widest text-black pb-2 sm:hidden">Challenge</h3>
										<p class="text-2xl font-medium leading-tight uppercase text-black pb-12 lg:pb-0 sm:text-xl">Small margins when outsourcing locally</p>
										<a href="#case-study-3" class="inline-flex justify-center w-64 text-xl font-semibold uppercase tracking-widest text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors hover:text-white hover:bg-purple lg:hidden caseBtn">Push for more</a>
									</div>
								</div>
								<img src="{{ asset('images/pen-paper-22.png') }}" class="absolute top-1/2 right-40 w-auto h-[40rem] transform -translate-y-1/2 lg:hidden">
								<img src="{{ asset('images/pen-paper-22-m.png') }}" class="hidden lg:block lg:m-auto lg:h-[24rem] lg:mb-24 sm:mb-12 xs:h-[20rem]">
								<a href="#case-study-3" class="justify-center w-64 text-xl font-semibold uppercase tracking-widest text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors hidden hover:text-white hover:bg-purple lg:inline-flex sm:text-lg caseBtn">Push for more</a>
							</div>
						</div>
					</div>
					<div class="relative bg-green lg:py-24 caseSlide">
						<div class="flex items-center w-full h-[56rem] lg:h-auto lg:text-center">
							<div class="relative max-w-7xl w-full px-4 m-auto">
								<div class="relative flex items-center lg:justify-center z-10">
									<div class="w-[28rem] lg:w-full lg:pb-24 sm:pb-12">
										<h3 class="text-sm font-medium uppercase tracking-widest text-black pb-4 sm:hidden"></h3>
										<h2 class="text-6xl font-bold leading-none uppercase text-black pb-6 sm:text-5xl">Awards making noise</h2>
										<h3 class="text-sm font-medium uppercase tracking-widest text-black pb-2 sm:hidden"></h3>
										<p class="text-2xl font-medium leading-tight uppercase text-black pb-12 lg:pb-0 sm:text-xl">Showcase of local and international  creativity and effectivity accolades</p>
										<a href="#case-study-4" class="inline-flex justify-center w-64 text-xl font-semibold uppercase tracking-widest text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors hover:text-white hover:bg-purple lg:hidden caseBtn">Press play</a>
									</div>
								</div>
								<img src="{{ asset('images/pen-paper-30.png') }}" class="absolute top-1/2 right-40 w-auto h-[40rem] transform -translate-y-1/2 lg:hidden">
								<img src="{{ asset('images/pen-paper-30-m.png') }}" class="hidden lg:block lg:m-auto lg:h-[24rem] lg:mb-24 sm:mb-12 xs:h-[20rem]">
								<a href="#case-study-4" class="justify-center w-64 text-xl font-semibold uppercase tracking-widest text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors hidden hover:text-white hover:bg-purple lg:inline-flex sm:text-lg caseBtn">Press play</a>
							</div>
						</div>
					</div>
					<div class="relative bg-yellow lg:py-24 caseSlide">
						<div class="flex items-center w-full h-[56rem] lg:h-auto lg:text-center">
							<div class="relative max-w-7xl w-full px-4 m-auto">
								<div class="relative flex items-center lg:justify-center z-10">
									<div class="w-96 lg:w-full lg:pb-24 sm:pb-12">
										<h3 class="text-sm font-medium uppercase tracking-widest text-black pb-4 sm:hidden"></h3>
										<h2 class="text-6xl font-bold leading-none uppercase text-black pb-6 sm:text-5xl">High-level skills</h2>
										<h3 class="text-sm font-medium uppercase tracking-widest text-black pb-2 sm:hidden"></h3>
										<p class="text-2xl font-medium leading-tight uppercase text-black pb-12 lg:pb-0 sm:text-xl">Check out examples of brands and agencies our experts worked with</p>
										<a href="#case-study-5" class="inline-flex justify-center w-64 text-xl font-semibold uppercase tracking-widest text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors hover:text-white hover:bg-purple lg:hidden caseBtn">Click to start</a>
									</div>
								</div>
								<img src="{{ asset('images/pen-paper-31.png') }}" class="absolute top-1/2 right-40 w-auto h-[40rem] transform -translate-y-1/2 lg:hidden">
								<img src="{{ asset('images/pen-paper-31-m.png') }}" class="hidden lg:block lg:m-auto lg:h-[24rem] lg:mb-24 sm:mb-12 xs:h-[20rem]">
								<a href="#case-study-5" class="justify-center w-64 text-xl font-semibold uppercase tracking-widest text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors hidden hover:text-white hover:bg-purple lg:inline-flex sm:text-lg caseBtn">Click to start</a>
							</div>
						</div>
					</div>
				</div>
				<div id="case-study-1" class="absolute top-0 left-0 right-0 bottom-0 flex items-center bg-black overflow-hidden z-10 lg:fixed lg:items-start lg:py-24 lg:overflow-x-hidden lg:overflow-y-auto lg:z-50 caseDetail" s
				tyle="display: none">
					<video class="absolute top-1/2 left-1/2 max-w-none min-w-full min-h-full transform -translate-x-1/2 -translate-y-1/2 lg:min-h-[200vh]" autoplay loop muted playsinline>
						<source src="{{ asset('videos/pen-paper-1.mp4') }}" type="video/mp4">
					</video>
					<button type="button" class="hidden absolute top-4 right-4 text-white transition-colors duration-100 z-10 hover:text-purple lg:block caseClose">
						<svg aria-hidden="true" focusable="false" class="w-8 h-8 fill-current">
							<use xlink:href="{{ asset('images/icons.svg#close') }}" />
						</svg>
					</button>
					<div class="max-w-7xl w-full px-12 mx-auto z-10 lg:px-4">
						<div class="flex -m-12 lg:flex-col lg:items-start lg:-m-6">
							<div class="w-1/2 p-12 lg:w-full lg:p-6">
								<h3 class="text-sm font-bold uppercase tracking-widest text-white pb-6">Roadmap</h3>
								<p class="text-3xl leading-normal text-white pb-12 lg:pb-0 md:text-2xl sm:text-xl xs:text-lg">Assembling a team of two Senior Digital Strategists, a UX designer, and a Creative Technologist on a Wednesday, we assisted the agency CEO and internal team with delivering <strong>50 pages of digital strategy and web, app, and microsite UI / UX by the following Sunday</strong>.</p>
								<button type="button" class="relative flex justify-between items-center w-64 text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors z-10 hover:text-white hover:bg-purple lg:hidden sm:m-auto caseClose">
									<span class="font-semibold uppercase tracking-widest">Back</span>
									<svg aria-hidden="true" focusable="false" class="w-4 h-6 fill-current">
										<use href="{{ asset('images/icons.svg#arrow-left') }}" />
									</svg>
								</button>
							</div>
							<div class="w-1/2 p-12 lg:w-full lg:p-6">
								<h3 class="text-sm font-bold uppercase tracking-widest text-white pb-6">Result</h3>
								<p class="text-5xl font-semibold uppercase leading-tight text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pb-6 md:text-5xl sm:text-4xl sm:pb-0 xs:text-3xl">The agency did not miss out on signing <strong>a new account with a leading brand</strong> in the ballpark of <strong>1 million €</strong>.</p>
							</div>
						</div>
					</div>
				</div>
				<div id="case-study-2" class="absolute top-0 left-0 right-0 bottom-0 flex items-center bg-black overflow-hidden z-10 lg:fixed lg:items-start lg:py-24 lg:overflow-x-hidden lg:overflow-y-auto lg:z-50 caseDetail" style="display: none">
					<video class="absolute top-1/2 left-1/2 max-w-none min-w-full min-h-full transform -translate-x-1/2 -translate-y-1/2 lg:min-h-[200vh]" autoplay loop muted playsinline>
						<source src="{{ asset('videos/pen-paper-2.mp4') }}" type="video/mp4">
					</video>
					<button type="button" class="hidden absolute top-4 right-4 text-white transition-colors duration-100 z-10 hover:text-purple lg:block caseClose">
						<svg aria-hidden="true" focusable="false" class="w-8 h-8 fill-current">
							<use xlink:href="{{ asset('images/icons.svg#close') }}" />
						</svg>
					</button>
					<div class="max-w-7xl w-full px-12 mx-auto z-10 lg:px-4">
						<div class="flex -m-12 lg:flex-col lg:items-start lg:-m-6">
							<div class="w-1/2 p-12 lg:w-full lg:p-6">
								<h3 class="text-sm font-bold uppercase tracking-widest text-white pb-6">Roadmap</h3>
								<p class="text-3xl leading-normal text-white pb-12 lg:pb-0 md:text-2xl sm:text-xl xs:text-lg">Our local agency partner <strong>won its first international megabrand pitch</strong>. Although a huge success, it also implied serious vacancies in the team. Our senior experts mixed well with the agency staff from day one and supported the delivery of key strategic tasks during a period of fast growth.</p>
								<button type="button" class="relative flex justify-between items-center w-64 text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors z-10 hover:text-white hover:bg-purple lg:hidden sm:m-auto caseClose">
									<span class="font-semibold uppercase tracking-widest">Back</span>
									<svg aria-hidden="true" focusable="false" class="w-4 h-6 fill-current">
										<use href="{{ asset('images/icons.svg#arrow-left') }}" />
									</svg>
								</button>
							</div>
							<div class="w-1/2 p-12 lg:w-full lg:p-6">
								<h3 class="text-sm font-bold uppercase tracking-widest text-white pb-6">Result</h3>
								<p class="text-5xl font-semibold uppercase leading-tight text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pb-6 md:text-5xl sm:text-4xl sm:pb-0 xs:text-3xl">The agency delivered strategy and execution for social, influencer, e-comm, and content to the full satisfaction of its new client. </p>
							</div>
						</div>
					</div>
				</div>
				<div id="case-study-3" class="absolute top-0 left-0 right-0 bottom-0 flex items-center bg-black overflow-hidden z-10 lg:fixed lg:items-start lg:py-24 lg:overflow-x-hidden lg:overflow-y-auto lg:z-50 caseDetail" style="display: none">
					<video class="absolute top-1/2 left-1/2 max-w-none min-w-full min-h-full transform -translate-x-1/2 -translate-y-1/2 lg:min-h-[200vh]" autoplay loop muted playsinline>
						<source src="{{ asset('videos/pen-paper-3.mp4') }}" type="video/mp4">
					</video>
					<button type="button" class="hidden absolute top-4 right-4 text-white transition-colors duration-100 z-10 hover:text-purple lg:block caseClose">
						<svg aria-hidden="true" focusable="false" class="w-8 h-8 fill-current">
							<use xlink:href="{{ asset('images/icons.svg#close') }}" />
						</svg>
					</button>
					<div class="max-w-7xl w-full px-12 mx-auto z-10 lg:px-4">
						<div class="flex -m-12 lg:flex-col lg:items-start lg:-m-6">
							<div class="w-1/2 p-12 lg:w-full lg:p-6">
								<h3 class="text-sm font-bold uppercase tracking-widest text-white pb-6">Roadmap</h3>
								<p class="text-3xl leading-normal text-white pb-12 lg:pb-0 md:text-2xl sm:text-xl xs:text-lg">An ATL agency tasked us with the desire to outsource programming, animation, and motion efficiently. We approached a <strong>previously tested, proven studio partner</strong> with an <strong>awarded team of tech and design geeks</strong>. After two and a half months of intensive cooperation, we helped deliver a <strong>world-class app experience</strong> for their client.</p>
								<button type="button" class="relative flex justify-between items-center w-64 text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors z-10 hover:text-white hover:bg-purple lg:hidden sm:m-auto caseClose">
									<span class="font-semibold uppercase tracking-widest">Back</span>
									<svg aria-hidden="true" focusable="false" class="w-4 h-6 fill-current">
										<use href="{{ asset('images/icons.svg#arrow-left') }}" />
									</svg>
								</button>
							</div>
							<div class="w-1/2 p-12 lg:w-full lg:p-6">
								<h3 class="text-sm font-bold uppercase tracking-widest text-white pb-6">Result</h3>
								<p class="text-5xl font-semibold uppercase leading-tight text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pb-6 md:text-5xl sm:text-4xl sm:pb-0 xs:text-3xl">Outsourcing the development process to <strong>our vetted provider, the agency tripled their profits</strong> due to their local market prices.</p>
							</div>
						</div>
					</div>
				</div>
				<div id="case-study-4" class="absolute top-0 left-0 right-0 bottom-0 flex items-center bg-black overflow-hidden z-10 lg:fixed lg:items-start lg:py-24 lg:overflow-x-hidden lg:overflow-y-auto lg:z-50 caseDetail" style="display: none">
					<video class="absolute top-1/2 left-1/2 max-w-none min-w-full min-h-full transform -translate-x-1/2 -translate-y-1/2 lg:min-h-[200vh]" autoplay loop muted playsinline>
						<source src="{{ asset('videos/pen-paper-4.mp4') }}" type="video/mp4">
					</video>
					<button type="button" class="hidden absolute top-4 right-4 text-white transition-colors duration-100 z-10 hover:text-purple lg:block caseClose">
						<svg aria-hidden="true" focusable="false" class="w-8 h-8 fill-current">
							<use xlink:href="{{ asset('images/icons.svg#close') }}" />
						</svg>
					</button>
					<div class="max-w-7xl w-full px-12 mx-auto z-10 lg:px-4">
						<ul class="pb-12 lg:pb-0">
							<li class="flex items-center xl:flex-col xl:items-start xl:pb-4">
								<span class="text-6xl font-semibold uppercase leading-none text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pr-6 xl:text-5xl xl:pr-0 sm:text-4xl">EFFIE</span>
								<span class="text-sm font-medium uppercase tracking-widest text-white">GOLD | SILVER | BRONZE WINNERS</span>
							</li>
							<li class="flex items-center xl:flex-col xl:items-start xl:pb-4">
								<span class="text-6xl font-semibold uppercase leading-none text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pr-6 xl:text-5xl xl:pr-0 sm:text-4xl">YOUTUBE WORKS</span>
								<span class="text-sm font-medium uppercase tracking-widest text-white">GRAND PRIX AND GOLD</span>
							</li>
							<li class="flex items-center xl:flex-col xl:items-start xl:pb-4">
								<span class="text-6xl font-semibold uppercase leading-none text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pr-6 xl:text-5xl xl:pr-0 sm:text-4xl">ART DIRECTORS CLUB EUROPE</span>
								<span class="text-sm font-medium uppercase tracking-widest text-white">SILVER FOR CRAFT </span>
							</li>
							<li class="flex items-center xl:flex-col xl:items-start xl:pb-4">
								<span class="text-6xl font-semibold uppercase leading-none text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pr-6 xl:text-5xl xl:pr-0 sm:text-4xl">ORANGE ADVERTISING AWARD</span>
								<span class="text-sm font-medium uppercase tracking-widest text-white"> BEST OUTDOOR</span>
							</li>
							<li class="flex items-center xl:flex-col xl:items-start xl:pb-4">
								<span class="text-6xl font-semibold uppercase leading-none text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pr-6 xl:text-5xl xl:pr-0 sm:text-4xl">GOLDEN NAIL</span>
								<span class="text-sm font-medium uppercase tracking-widest text-white">MULTIPLE GOLD | SLIVER | BRONZE AWARDS FOR ATL & DIGITAL</span>
							</li>
							<li class="flex items-center xl:flex-col xl:items-start xl:pb-4">
								<span class="text-6xl font-semibold uppercase leading-none text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pr-6 xl:text-5xl xl:pr-0 sm:text-4xl">GOLDEN HAMMER</span>
								<span class="text-sm font-medium uppercase tracking-widest text-white">GOLD IN BEST USE OF SPONSORSHIP</span>
							</li>
							<li class="flex items-center xl:flex-col xl:items-start xl:pb-4">
								<span class="text-6xl font-semibold uppercase leading-none text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pr-6 xl:text-5xl xl:pr-0 sm:text-4xl">DIGITAL PIE</span>
								<span class="text-sm font-medium uppercase tracking-widest text-white">GOLD & SILVER</span>
							</li>
							<li class="flex items-center xl:flex-col xl:items-start xl:pb-4">
								<span class="text-6xl font-semibold uppercase leading-none text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pr-6 xl:text-5xl xl:pr-0 sm:text-4xl">NEW YORK FESTIVALS</span>
								<span class="text-sm font-medium uppercase tracking-widest text-white">MULTIPLE GLOBAL FINALISTS</span>
							</li>
							<li class="flex items-center xl:flex-col xl:items-start">
								<span class="text-6xl font-semibold uppercase leading-none text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pr-6 xl:text-5xl xl:pr-0 sm:text-4xl">YOUNG LIONS</span>
								<span class="text-sm font-medium uppercase tracking-widest text-white">MULTIPLE NATIONAL WINNERS</span>
							</li>
						</ul>
						<button type="button" class="relative flex justify-between items-center w-64 text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors z-10 hover:text-white hover:bg-purple lg:hidden sm:m-auto caseClose">
							<span class="font-semibold uppercase tracking-widest">Back</span>
							<svg aria-hidden="true" focusable="false" class="w-4 h-6 fill-current">
								<use href="{{ asset('images/icons.svg#arrow-left') }}" />
							</svg>
						</button>
					</div>
				</div>
				<div id="case-study-5" class="absolute top-0 left-0 right-0 bottom-0 flex items-center bg-black overflow-hidden z-10 lg:fixed lg:items-start lg:py-24 lg:overflow-x-hidden lg:overflow-y-auto lg:z-50 caseDetail" style="display: none">
					<video class="absolute top-1/2 left-1/2 max-w-none min-w-full min-h-full transform -translate-x-1/2 -translate-y-1/2 lg:min-h-[200vh]" autoplay loop muted playsinline>
						<source src="{{ asset('videos/pen-paper-5.mp4') }}" type="video/mp4">
					</video>
					<button type="button" class="hidden absolute top-4 right-4 text-white transition-colors duration-100 z-10 hover:text-purple lg:block caseClose">
						<svg aria-hidden="true" focusable="false" class="w-8 h-8 fill-current">
							<use xlink:href="{{ asset('images/icons.svg#close') }}" />
						</svg>
					</button>
					<div class="max-w-7xl w-full px-12 mx-auto z-10 lg:px-4">
						<div class="pb-24 lg:pb-0">
							<div class="flex flex-wrap items-center -m-10 lg:-m-6">
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/coca-cola.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/heineken.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/vitamin-well.png') }}" class="inline-block max-w-[8rem] max-h-[4rem] lg:max-w-[6rem] sm:max-h-[4rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/slovak-telekom-2.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/slido.png') }}" class="inline-block max-w-[8rem] max-h-[4rem] lg:max-w-[6rem] sm:max-h-[4rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/skoda.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/suzuki.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/canon.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/electrolux.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/raiffeisen-bank.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/lidl.png') }}" class="inline-block max-w-[10rem] max-h-[6rem] lg:max-w-[8rem] sm:max-h-[4rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/nocco.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/amazon.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/orange.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/boehringer-ingelheim.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/strv.png') }}" class="inline-block max-w-[10rem] max-h-[3rem] lg:max-w-[8rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/ogilvy.png') }}" class="inline-block max-w-[10rem] max-h-[3rem] lg:max-w-[8rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/leo-burnett.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/wmc-grey.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
								<span class="w-1/5 text-center p-10 lg:w-1/4 lg:p-6 md:w-1/3 sm:w-1/2">
									<img src="{{ asset('images/logos/mark-bbdo.png') }}" class="inline-block max-w-[10rem] max-h-[4rem] lg:max-w-[8rem] lg:max-h-[3rem] sm:max-h-[2rem]">
								</span>
							</div>
						</div>
						<button type="button" class="relative flex justify-between items-center w-64 text-black bg-white pl-8 py-3 pr-6 rounded-full transition-colors z-10 hover:text-white hover:bg-purple lg:hidden sm:m-auto caseClose">
							<span class="font-semibold uppercase tracking-widest">Back</span>
							<svg aria-hidden="true" focusable="false" class="w-4 h-6 fill-current">
								<use href="{{ asset('images/icons.svg#arrow-left') }}" />
							</svg>
						</button>
					</div>
				</div>
			</div>
		</section>
		<section class="relative bg-gray-200 py-36 sm:py-24">
			<div class="relative max-w-5xl w-full px-12 m-auto z-20 lg:px-4">
				<img src="{{ asset('images/pen-paper-14.png') }}" class="absolute top-[16%] -left-1/4 lg:top-[40%] lg:left-1/2 lg:transform lg:-translate-x-1/2">
				<img src="{{ asset('images/pen-paper-15.png') }}" class="absolute top-0 right-[-15%] lg:-left-1/3 lg:right-auto">
				<div class="relative pb-36 lg:pb-24 sm:pb-6 z-10">
					<div class="flex justify-between items-end lg:flex-col lg:justify-start lg:items-center">
						<h2 class="text-8xl font-semibold leading-none uppercase text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink lg:text-center lg:pb-0 md:text-7xl md:leading-none sm:text-6xl sm:leading-none xs:text-5xl">What people <br>like you think <br>about us</h2>
						<div class="flex items-center z-10 lg:hidden">
							<button type="button" class="inline-flex justify-center items-center w-14 h-14 text-white mr-4 border border-white rounded-full transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple refSliderPrev">
								<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
									<use href="{{ asset('images/icons.svg#arrow-left') }}" />
								</svg>
							</button>
							<button type="button" class="inline-flex justify-center items-center w-14 h-14 text-white border border-white rounded-full transition-colors duration-100 hover:text-white hover:bg-purple hover:border-purple refSliderNext">
								<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
									<use href="{{ asset('images/icons.svg#arrow-right') }}" />
								</svg>
							</button>
						</div>
					</div>
				</div>
				<div class="relative">
					<button type="button" class="absolute top-16 left-0 text-white z-10 hidden lg:block sm:top-12 refSliderPrev">
						<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
							<use href="{{ asset('images/icons.svg#arrow-left') }}" />
						</svg>
					</button>
					<button type="button" class="absolute top-16 right-0 text-white z-10 hidden lg:block sm:top-12 refSliderNext">
						<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
							<use href="{{ asset('images/icons.svg#arrow-right') }}" />
						</svg>
					</button>
					<div class="refSliderSlides">
						<div>
							<div class="flex items-start lg:flex-col lg:items-center lg:text-center">
								<img src="{{ asset('images/martin-motacek.png') }}" alt="Martin Motáček" class="flex-shrink-0 w-40 mr-12 lg:mr-0 lg:mb-12 sm:w-32 sm:mb-6">
								<div class="flex flex-col w-full">
									<div class="text-sm font-medium uppercase tracking-widest text-white text-opacity-20 pb-4 lg:order-1 sm:pb-2">Advertising agency</div>
									<blockquote class="text-5xl font-medium leading-tight uppercase text-white pb-6 md:text-4xl sm:text-3xl lg:order-4">“Pen & Paper has found some of the best creative minds, bound them together, and put them into your service.”</blockquote>
									<div class="font-medium uppercase tracking-widest text-white pb-2 lg:order-2">Martin Motáček</div>
									<div class="text-sm text-white text-opacity-60 lg:order-3 lg:pb-6">Creative Director, <br class="lg:hidden"><strong>Wiktor Leo Burnett</strong></div>
								</div>
							</div>
						</div>
						<div>
							<div class="flex items-start lg:flex-col lg:items-center lg:text-center">
								<img src="{{ asset('images/petra-danielovicova.png') }}" alt="Petra Danielovičová" class="flex-shrink-0 w-40 mr-12 lg:mr-0 lg:mb-12 sm:w-32 sm:mb-6">
								<div class="flex flex-col w-full">
									<div class="text-sm font-medium uppercase tracking-widest text-white text-opacity-20 pb-4 lg:order-1 sm:pb-2">Advertising agency</div>
									<blockquote class="text-5xl font-medium leading-tight uppercase text-white pb-6 md:text-4xl sm:text-3xl lg:order-4">“I consider the people from Pen & Paper as utmost professionals that deliver on their promise of effective collaboration.”</blockquote>
									<div class="font-medium uppercase tracking-widest text-white pb-2 lg:order-2">Petra Danielovičová</div>
									<div class="text-sm text-white text-opacity-60 lg:order-3 lg:pb-6">Head of Online, <br class="lg:hidden"><strong>Dotcom Advertising (Tesco's CE Digital Agency)</strong></div>
								</div>
							</div>
						</div>
						<div>
							<div class="flex items-start lg:flex-col lg:items-center lg:text-center">
								<img src="{{ asset('images/michal-mazar.png') }}" alt="Michal Mažár" class="flex-shrink-0 w-40 mr-12 lg:mr-0 lg:mb-12 sm:w-32 sm:mb-6">
								<div class="flex flex-col w-full">
									<div class="text-sm font-medium uppercase tracking-widest text-white text-opacity-20 pb-4 lg:order-1 sm:pb-2">Telecommunications brand</div>
									<blockquote class="text-5xl font-medium leading-tight uppercase text-white pb-6 md:text-4xl sm:text-3xl lg:order-4">“Our collaboration with Pen & Paper was professional, highly effective, and reflected the given brief precisely.”</blockquote>
									<div class="font-medium uppercase tracking-widest text-white pb-2 lg:order-2">Michal Mažár</div>
									<div class="text-sm text-white text-opacity-60 lg:order-3 lg:pb-6">Marketing and Sales Director, <br><strong>DIGI Slovakia, Telekom Group</strong></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="why-should-you-care" class="text-center bg-gray-300 py-36 sm:py-24">
			<div class="max-w-7xl w-full px-4 m-auto 3xl:max-w-5xl">
				<h2 class="text-8xl font-semibold uppercase leading-none text-transparent bg-clip-text bg-gradient-to-b from-purple to-pink pb-12 md:text-7xl md:leading-none sm:text-6xl sm:leading-none xs:text-5xl">Why should <br>you care?</h2>
				<p class="text-2xl font-extralight text-white pb-24">Pen & Paper has been designed from the get-go to <strong>make your life easier.</strong> <br>Our experts are great for both quick fixes, and for building long-term partnerships.</p>
				<div class="flex flex-wrap justify-center -m-12 3xl:items-center">
					<div class="w-1/2 p-12 3xl:w-full">
						<div class="3xl:max-w-[36rem] 3xl:mx-auto">
							<svg aria-hidden="true" focusable="false" class="inline-block w-auto h-16 mb-4 fill-current text-purple">
								<use href="{{ asset('images/icons.svg#watch') }}" />
							</svg>
							<h3 class="text-2xl font-semibold uppercase tracking-widest text-white pb-4">Fix vacancies quicker</h3>
							<div class="text-lg leading-relaxed text-white text-opacity-60">Skip the complicated recruitment process. In lightning <strong>48 hours</strong>, we will present you with a variety of our proven talents. <strong><br>No recruitment fees.</strong></div>
						</div>
					</div>
					<div class="w-1/2 p-12 3xl:w-full">
						<div class="3xl:max-w-[36rem] 3xl:mx-auto">
							<svg aria-hidden="true" focusable="false" class="inline-block w-auto h-16 mb-4 fill-current text-purple">
								<use href="{{ asset('images/icons.svg#chip') }}" />
							</svg>
							<h3 class="text-2xl font-semibold uppercase tracking-widest text-white pb-4">Expand your capabilities </h3>
							<div class="text-lg leading-relaxed text-white text-opacity-60">Want to try something new or expand your skill set? Get some of our <strong>niche specialists</strong> on board and grow your in-house team's expertise gradually.</div>
						</div>
					</div>
					<div class="w-1/2 p-12 3xl:w-full">
						<div class="3xl:max-w-[36rem] 3xl:mx-auto">
							<svg aria-hidden="true" focusable="false" class="inline-block w-auto h-16 mb-4 fill-current text-purple">
								<use href="{{ asset('images/icons.svg#graph') }}" />
							</svg>
							<h3 class="text-2xl font-semibold uppercase tracking-widest text-white pb-4">Scale your business</h3>
							<div class="text-lg leading-relaxed text-white text-opacity-60">Kick-starting growth and looking for <strong>external partners</strong> to help you scale <strong>without management costs</strong>? Build a <strong>talent network</strong> for years to come with us.</div>
						</div>
					</div>
					<div class="w-1/2 p-12 3xl:w-full">
						<div class="3xl:max-w-[36rem] 3xl:mx-auto">
							<svg aria-hidden="true" focusable="false" class="inline-block w-auto h-16 mb-4 fill-current text-purple">
								<use href="{{ asset('images/icons.svg#user') }}" />
							</svg>
							<h3 class="text-2xl font-semibold uppercase tracking-widest text-white pb-4">Hire whole studios</h3>
							<div class="text-lg leading-relaxed text-white text-opacity-60">We also provide you with <strong>selected independent studios</strong> to deliver on your UX, 3D, or programming needs. Led by our <strong>Account Directors with agency experience</strong>.</div>
						</div>
					</div>
					<div class="w-1/2 p-12 3xl:w-full">
						<div class="3xl:max-w-[36rem] 3xl:mx-auto">
							<svg aria-hidden="true" focusable="false" class="inline-block w-auto h-16 mb-4 fill-current text-purple">
								<use href="{{ asset('images/icons.svg#cheque') }}" />
							</svg>
							<h3 class="text-2xl font-semibold uppercase tracking-widest text-white pb-4">Double your output</h3>
							<div class="text-lg leading-relaxed text-white text-opacity-60">Our timezone conveniently supplements both EST and WST and gives you a unique opportunity to super-boost the production of your outcomes.</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="what-we-do" class="relative bg-gradient-to-b from-purple to-pink py-56 lg:py-36 sm:py-24">
			<div class="relative max-w-7xl w-full px-4 m-auto z-20">
				<div class="flex items-start -m-32 xl:flex-wrap xl:-m-8">
					<div class="relative flex-shrink-0 w-[38rem] p-32 mt-12 xl:w-full xl:mt-0 xl:p-8">
						<div class="absolute top-1/2 -right-20 w-[3200px] h-[36rem] border border-l-0 border-white border-opacity-20 rounded-r-full transform -translate-y-1/2 xl:hidden"></div>
						<h2 class="text-8xl font-bold leading-tight uppercase text-white pb-6 md:text-7xl sm:text-6xl">What <br>we do</h2>
						<div class="text-2xl font-light leading-normal text-white sm:text-xl">All of our people are experts in their fields and act as consultants, rather than just delivery drones.</div>
					</div>
					<div class="p-32 xl:w-full xl:p-8">
						<ul>
							<li class="pb-4 plus">
								<button type="button" class="flex items-center text-left text-white text-opacity-60 transition-colors duration-100 plusBtn">
									<span class="text-5xl font-bold leading-none uppercase md:text-4xl">Ideation & <br>Creativity</span>
									<span class="relative flex-shrink-0 w-14 h-14 ml-6 border border-white rounded-full opacity-0 transition-colors duration-100 sm:w-12 sm:h-12 plusBtnIcon">
										<span class="absolute top-1/2 left-1/2 w-6 h-px bg-white transform -translate-x-1/2 -translate-y-1/2"></span>
										<span class="absolute top-1/2 left-1/2 w-px h-6 bg-white transform -translate-x-1/2 -translate-y-1/2 plusBtnIconLine"></span>
									</span>
								</button>
								<p class="text-lg leading-normal text-white pt-3 pb-6 plusMain" style="display: none">Discover fresh ideas with senior strategists & creative directors from the European advertising business.</p>
							</li>
							<li class="pb-4 plus">
								<button type="button" class="flex items-center text-left text-white text-opacity-60 transition-colors duration-100 plusBtn">
									<span class="text-5xl font-bold leading-none uppercase md:text-4xl">Product design</span>
									<span class="relative flex-shrink-0 w-14 h-14 ml-6 border border-white rounded-full opacity-0 transition-colors duration-100 sm:w-12 sm:h-12 plusBtnIcon">
										<span class="absolute top-1/2 left-1/2 w-6 h-px bg-white transform -translate-x-1/2 -translate-y-1/2"></span>
										<span class="absolute top-1/2 left-1/2 w-px h-6 bg-white transform -translate-x-1/2 -translate-y-1/2 plusBtnIconLine"></span>
									</span>
								</button>
								<p class="text-lg leading-normal text-white pt-3 pb-6 plusMain" style="display: none">Plan, develop and validate new services & products with data scientists and business analysts boasting an <strong>amazing track record with</strong> boldly <strong>innovative companies</strong>.</p>
							</li>
							<li class="pb-4 plus">
								<button type="button" class="flex items-center text-left text-white text-opacity-60 transition-colors duration-100 plusBtn">
									<span class="text-5xl font-bold leading-none uppercase md:text-4xl">Art direction</span>
									<span class="relative flex-shrink-0 w-14 h-14 ml-6 border border-white rounded-full opacity-0 transition-colors duration-100 sm:w-12 sm:h-12 plusBtnIcon">
										<span class="absolute top-1/2 left-1/2 w-6 h-px bg-white transform -translate-x-1/2 -translate-y-1/2"></span>
										<span class="absolute top-1/2 left-1/2 w-px h-6 bg-white transform -translate-x-1/2 -translate-y-1/2 plusBtnIconLine"></span>
									</span>
								</button>
								<p class="text-lg leading-normal text-white pt-3 pb-6 plusMain" style="display: none">Start drafting bold designs with art directors, brand experts, and specialized designers who see and think differently.</p>
							</li>
							<li class="pb-4 plus">
								<button type="button" class="flex items-center text-left text-white text-opacity-60 transition-colors duration-100 plusBtn">
									<span class="text-5xl font-bold leading-none uppercase md:text-4xl">Digital <br>development</span>
									<span class="relative flex-shrink-0 w-14 h-14 ml-6 border border-white rounded-full opacity-0 transition-colors duration-100 sm:w-12 sm:h-12 plusBtnIcon">
										<span class="absolute top-1/2 left-1/2 w-6 h-px bg-white transform -translate-x-1/2 -translate-y-1/2"></span>
										<span class="absolute top-1/2 left-1/2 w-px h-6 bg-white transform -translate-x-1/2 -translate-y-1/2 plusBtnIconLine"></span>
									</span>
								</button>
								<p class="text-lg leading-normal text-white pt-3 pb-6 plusMain" style="display: none">Spanning the whole portfolio of platforms, our development teams deliver <strong>flawless function, lightning speed and exceptional reliability</strong>.</p>
							</li>
							<li class="plus">
								<button type="button" class="flex items-center text-left text-white text-opacity-60 transition-colors duration-100 plusBtn">
									<span class="text-5xl font-bold leading-none uppercase md:text-4xl">Strategy</span>
									<span class="relative flex-shrink-0 w-14 h-14 ml-6 border border-white rounded-full opacity-0 transition-colors duration-100 sm:w-12 sm:h-12 plusBtnIcon">
										<span class="absolute top-1/2 left-1/2 w-6 h-px bg-white transform -translate-x-1/2 -translate-y-1/2"></span>
										<span class="absolute top-1/2 left-1/2 w-px h-6 bg-white transform -translate-x-1/2 -translate-y-1/2 plusBtnIconLine"></span>
									</span>
								</button>
								<p class="text-lg leading-normal text-white pt-3 pb-6 plusMain" style="display: none">A range of strategists with complex brand and digital know-how are at your disposal to see and plan for years ahead in e-commerce, B2B, digital, influencer, or content marketing. </p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section id="how-it-works" class="bg-gray-300 bg-cover bg-top bg-no-repeat py-56 sm:py-24" style="background-image: url({{ asset('images/pen-paper-2.jpg') }})">
			<div class="max-w-7xl w-full px-12 m-auto lg:px-4">
				<div class="pb-24 sm:pb-16">
					<h2 class="text-8xl font-semibold uppercase text-center text-white md:text-7xl sm:text-6xl">Get <span class="relative">Shit<img src="{{ asset('images/pen-paper-16.png') }}" class="absolute -top-4 -right-1 z-20 md:-top-3 md:w-32 sm:-top-3 sm:w-28 sm:right-1"></span> Done</h2>
				</div>
				<div class="flex flex-wrap -m-8">
					<div class="relative w-1/3 p-8 lg:w-full">
						<div class="absolute top-20 left-8 w-1/2 h-48 border-t border-r border-white border-opacity-20 rounded-r-full"></div>
						<div class="relative z-20">
							<span class="inline-block text-8xl font-semibold text-transparent bg-clip-text bg-gradient-to-tr from-purple to-pink pb-6">1</span>
							<h3 class="text-2xl font-semibold uppercase tracking-widest text-white pb-6">Intro meeting</h3>
							<div class="text-lg leading-relaxed text-white text-opacity-60">We'll meet your creative / account team, whip out our pens & paper, and listen to their briefing <strong>to understand what your project needs</strong>.</div>
						</div>
					</div>
					<div class="relative w-1/3 p-8 lg:w-full">
						<div class="absolute top-20 left-8 w-1/2 h-48 border-t border-r border-white border-opacity-20 rounded-r-full"></div>
						<div class="relative z-20">
							<span class="inline-block text-8xl font-semibold text-transparent bg-clip-text bg-gradient-to-tr from-purple to-pink pb-6">2</span>
							<h3 class="text-2xl font-semibold uppercase tracking-widest text-white pb-6">Expert onboarding</h3>
							<div class="text-lg leading-relaxed text-white text-opacity-60"><strong>In 48 hours or less</strong>, we'll get back to you with <strong>experts that fit your brief, timeline, and budget</strong>. An intro call may follow if you wish before briefing the details.</div>
						</div>
					</div>
					<div class="relative w-1/3 p-8 lg:w-full">
						<div class="absolute top-20 left-8 w-1/2 h-48 border-t border-r border-white border-opacity-20 rounded-r-full"></div>
						<div class="relative z-20">
							<span class="inline-block text-8xl font-semibold text-transparent bg-clip-text bg-gradient-to-tr from-purple to-pink pb-6">3</span>
							<h3 class="text-2xl font-semibold uppercase tracking-widest text-white pb-6">Continued support</h3>
							<div class="text-lg leading-relaxed text-white text-opacity-60">If needed, our team will support you with experts during the whole lifespan of your project. Larger, team-based projects are led by dedicated Account Directors to help you along.</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="contact" class="relative bg-white pt-80 pb-36 sm:py-24">
			<img src="{{ asset('images/pen-paper-17.png') }}" class="absolute top-96 -left-96">
			<img src="{{ asset('images/pen-paper-18.png') }}" class="absolute top-0 right-0 sm:-top-24 sm:left-1/2 sm:max-w-[35rem] sm:transform sm:-translate-x-1/2">
			<div class="absolute top-48 left-0 w-3/4 h-[30rem] border-t border-r border-gray-300 border-opacity-10 rounded-r-full md:hidden"></div>
			<div class="relative max-w-5xl w-full px-12 m-auto z-20 lg:px-4">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 331 26" class="w-40 h-auto fill-current text-black mb-6">
					<path d="M11.52,12.35A3.75,3.75,0,0,0,15.6,8.63c0-2.12-1.63-3.69-4.08-3.69H4.27v7.41ZM12.08,1A7.53,7.53,0,0,1,20,8.56c0,4.21-3.24,7.53-7.92,7.53H4.27V25H0V1Z"/>
					<polygon points="51.44 11.09 51.44 14.91 38.41 14.91 38.41 21.09 52 21.09 52 25 34 25 34 1 52 1 52 5 38.41 5 38.41 11.09 51.44 11.09"/>
					<polygon points="82.96 17.12 82.96 1.92 87 1.92 87 26 69.05 9.88 69.05 25.09 65 25.09 65 1 82.96 17.12"/>
					<path d="M143.47,11.79h-6.11a6.56,6.56,0,0,1,2.44,5.28c0,4.44-4,7.93-10.1,7.93-5.95,0-9.7-2.78-9.7-7.26a5.72,5.72,0,0,1,2.12-4.4,6.73,6.73,0,0,1-1.67-4.49c0-4.68,4.07-7.85,10.47-7.85a15,15,0,0,1,7.21,1.63v3.8A12.79,12.79,0,0,0,131,4.53c-3.83,0-6.27,1.75-6.27,4.48s2.56,4.57,5.91,4.57l-.45,3.13a11.83,11.83,0,0,1-5.58-1.35,3.93,3.93,0,0,0-.49,1.86c0,2.66,2.4,4.25,5.58,4.25,3.38,0,6-1.79,6-5,0-2.7-2.16-5-5.95-5.27V8.42H144Z"/>
					<path d="M190.52,12.35a3.74,3.74,0,0,0,4.08-3.72c0-2.12-1.63-3.69-4.08-3.69h-7.25v7.41ZM191.08,1A7.53,7.53,0,0,1,199,8.56c0,4.21-3.24,7.53-7.92,7.53h-7.81V25H179V1Z"/>
					<path d="M213.62,16.33h8.71L218,8.42ZM208.84,25H204L218,0l14,25h-4.86l-2.82-5.11H211.66Z"/>
					<path d="M256.52,12.35a3.75,3.75,0,0,0,4.08-3.72c0-2.12-1.63-3.69-4.08-3.69h-7.25v7.41ZM257.08,1A7.53,7.53,0,0,1,265,8.56c0,4.21-3.24,7.53-7.92,7.53h-7.81V25H245V1Z"/>
					<polygon points="295.44 11.09 295.44 14.91 282.41 14.91 282.41 21.09 296 21.09 296 25 278 25 278 1 296 1 296 5 282.41 5 282.41 11.09 295.44 11.09"/>
					<path d="M322.42,11.94a3.58,3.58,0,0,0,3.87-3.52,3.58,3.58,0,0,0-3.87-3.52h-7.24v7ZM315.18,25H311V1h11.9c4.59,0,7.71,3.27,7.71,7.34a7.12,7.12,0,0,1-6.32,7.11L331,25h-5l-6.39-9.3h-4.43Z"/>
				</svg>
				<h2 class="text-8xl font-semibold uppercase text-black pb-6 -ml-2 md:text-7xl sm:text-6xl xs:text-5xl">Let's Talk!</h2>
				<div class="text-xl leading-relaxed text-black pb-24 sm:text-lg sm:pb-16">Does the above sound familiar? Let's talk and see how we <br class="sm:hidden">might help you. It's up to you what we do next:</div>
				<div class="flex flex-wrap -m-8">
					<div class="w-1/2 p-8 md:w-full">
						<h3 class="text-3xl font-semibold uppercase pb-6">Request a demo</h3>
						<div class="text-xl h-40 leading-relaxed text-black mb-6 md:h-auto sm:text-lg"><strong>Questions?</strong> Let us show you some of our experts and the work they’ve done for clients like you. We’ll walk you through our own online platform and figure out if this makes sense for you.</div>
						<a href="#request-a-demo" class="flex justify-between items-center w-64 pl-8 py-3 pr-6 border-2 border-black rounded-full transition-colors hover:text-white hover:bg-purple hover:border-purple showFormBtn">
							<span class="font-semibold uppercase tracking-widest">Book now</span>
							<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
								<use href="{{ asset('images/icons.svg#arrow-right') }}" />
							</svg>
						</a>
					</div>
					<div class="w-1/2 p-8 md:w-full">
						<h3 class="text-3xl font-semibold uppercase pb-6">Brief an Expert</h3>
						<div class="text-xl h-40 leading-relaxed text-black mb-6 md:h-auto sm:text-lg"><strong>No time to spare?</strong> Book a meeting with our account manager and get <strong>right to onboarding talent for your project</strong>.</div>
						<a href="#brief-an-expert" class="flex justify-between items-center w-64 pl-8 py-3 pr-6 border-2 border-black rounded-full transition-colors hover:text-white hover:bg-purple hover:border-purple showFormBtn">
							<span class="font-semibold uppercase tracking-widest">Brief now</span>
							<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
								<use href="{{ asset('images/icons.svg#arrow-right') }}" />
							</svg>
						</a>
					</div>
				</div>
				<div id="request-a-demo" class="pt-24 sm:pt-12 showFormBox" style="display: none">
					<h3 class="text-2xl font-semibold uppercase pb-12">Request a demo</h3>
					<form action="{{ route('send') }}" method="POST" class="form">
						@csrf
						<input type="hidden" name="g-recaptcha-response" class="g-recaptcha-response">
						<input type="hidden" name="type" value="demo">
						<div class="flex flex-wrap -m-2">
							<div class="w-1/2 p-2 sm:w-full">
								<input type="text" name="demo_name" id="demo_name" class="w-full text-sm p-3 border border-black appearance-none focus:border-black" placeholder="Name">
								<div class="text-sm text-red pt-1 formError" role="alert" style="display: none"></div>
							</div>
							<div class="w-1/2 p-2 sm:w-full">
								<input type="email" name="demo_email" id="demo_email" class="w-full text-sm p-3 border border-black appearance-none focus:border-black" placeholder="Email">
								<div class="text-sm text-red pt-1 formError" role="alert" style="display: none"></div>
							</div>
							<div class="w-1/2 p-2 sm:w-full">
								<input type="text" name="demo_company" id="demo_company" class="w-full text-sm p-3 border border-black appearance-none focus:border-black" placeholder="Company">
								<div class="text-sm text-red pt-1 formError" role="alert" style="display: none"></div>
							</div>
							<div class="w-1/2 p-2 sm:w-full">
								<input type="text" name="demo_date" id="demo_date" class="w-full text-sm p-3 border border-black appearance-none focus:border-black" placeholder="Preffered date & time">
								<div class="text-sm text-red pt-1 formError" role="alert" style="display: none"></div>
							</div>
							<div class="w-full p-2">
								<textarea name="demo_notes" id="demo_notes" class="w-full text-sm p-3 border border-black appearance-none focus:border-black" placeholder="Anything to add?" rows="6"></textarea>
								<div class="text-sm text-red pt-1 formError" role="alert" style="display: none"></div>
							</div>
							<div class="flex justify-between items-center w-full p-2 sm:flex-col sm:justify-start sm:items-start">
								<span class="flex items-center sm:pb-4">
									<input type="checkbox" name="demo_agree" id="demo_agree" class="w-6 h-6 text-black border border-black">
									<label for="demo_agree" class="text-sm text-black pl-3">GDPR</label>
								</span>
								<button type="submit" class="flex justify-between items-center w-64 text-white bg-black pl-8 py-3 pr-6 rounded-full transition-colors hover:bg-purple">
									<span class="font-semibold uppercase tracking-widest">Send message</span>
									<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
										<use href="{{ asset('images/icons.svg#arrow-right') }}" />
									</svg>
								</button>
							</div>
						</div>
					</form>
				</div>
				<div id="brief-an-expert" class="pt-24 sm:pt-12 showFormBox" style="display: none">
					<h3 class="text-2xl font-semibold uppercase pb-12">Brief an Expert</h3>
					<form action="{{ route('send') }}" method="POST" class="form">
						@csrf
						<input type="hidden" name="g-recaptcha-response" class="g-recaptcha-response">
						<input type="hidden" name="type" value="expert">
						<div class="flex flex-wrap -m-2">
							<div class="w-1/2 p-2 sm:w-full">
								<input type="text" name="expert_name" id="expert_name" class="w-full text-sm p-3 border border-black appearance-none focus:border-black" placeholder="Name">
								<div class="text-sm text-red pt-1 formError" role="alert" style="display: none"></div>
							</div>
							<div class="w-1/2 p-2 sm:w-full">
								<input type="email" name="expert_email" id="expert_email" class="w-full text-sm p-3 border border-black appearance-none focus:border-black" placeholder="Email">
								<div class="text-sm text-red pt-1 formError" role="alert" style="display: none"></div>
							</div>
							<div class="w-full p-2">
								<input type="text" name="expert_company" id="expert_company" class="w-full text-sm p-3 border border-black appearance-none focus:border-black" placeholder="Company">
								<div class="text-sm text-red pt-1 formError" role="alert" style="display: none"></div>
							</div>
							<div class="w-1/2 p-2 sm:w-full">
								<input type="text" name="expert_project_size" id="expert_project_size" class="w-full text-sm p-3 border border-black appearance-none focus:border-black" placeholder="Project size">
								<div class="text-sm text-red pt-1 formError" role="alert" style="display: none"></div>
							</div>
							<div class="w-1/4 p-2 sm:w-1/2">
								<input type="radio" name="expert_owner" id="expert_owner_individual" value="Individual" class="hidden" checked>
								<label for="expert_owner_individual" class="inline-block w-full text-sm text-center p-3 border border-black appearance-none focus:border-black">Individual</label>
							</div>
							<div class="w-1/4 p-2 sm:w-1/2">
								<input type="radio" name="expert_owner" id="expert_owner_team" value="Team" class="hidden">
								<label for="expert_owner_team" class="inline-block w-full text-sm text-center p-3 border border-black appearance-none focus:border-black">Team</label>
							</div>
							<div class="w-full p-2">
								<textarea name="expert_project_details" id="expert_project_details" class="w-full text-sm p-3 border border-black appearance-none focus:border-black" placeholder="Project details" rows="6"></textarea>
								<div class="text-sm text-red pt-1 formError" role="alert" style="display: none"></div>
							</div>
							<div class="flex justify-between items-center w-full p-2 sm:flex-col sm:justify-start sm:items-start">
								<span class="flex items-center sm:pb-4">
									<input type="checkbox" name="expert_agree" id="expert_agree" class="w-6 h-6 text-black border border-black">
									<label for="expert_agree" class="text-sm text-black pl-3">GDPR</label>
								</span>
								<button type="submit" class="flex justify-between items-center w-64 text-white bg-black pl-8 py-3 pr-6 rounded-full transition-colors hover:bg-purple">
									<span class="font-semibold uppercase tracking-widest">Send message</span>
									<svg aria-hidden="true" focusable="false" class="w-6 h-4 fill-current">
										<use href="{{ asset('images/icons.svg#arrow-right') }}" />
									</svg>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>
	</main>
@endsection