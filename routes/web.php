<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Domovska stranka
Route::view('/', 'pages.home');

// Odoslanie poziadavky z formulara
Route::post('send', 'ContactController@send')->name('send');
