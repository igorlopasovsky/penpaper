const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
	mode: 'jit',
	purge: {
		content: [
			'./resources/**/*.blade.php',
			'./resources/**/*.js',
			'./resources/**/*.css',
		],
		safelist: []
	},
	darkMode: false,
	theme: {
		screens: {
			'3xl': { 'max': '1999.98px'},
			'2xl': { 'max': '1399.98px' },
			'xl': { 'max': '1199.98px' },
			'lg': { 'max': '991.98px' },
			'md': { 'max': '767.98px' },
			'sm': { 'max': '575.98px' },
			'xs': { 'max': '399.98px' }
		},
		colors: {
			transparent: 'transparent',
			current: 'currentColor',
			white: ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-white), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-white), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-white))`
			},
			black: ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-black), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-black), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-black))`
			},
			pink: ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-pink), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-pink), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-pink))`
			},
			purple: ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-purple), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-purple), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-purple))`
			},
			blue: ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-blue), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-blue), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-blue))`
			},
			red: ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-red), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-red), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-red))`
			},
			yellow: ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-yellow), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-yellow), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-yellow))`
			},
			green: ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-green), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-green), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-green))`
			},
			'gray-100': ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-gray-100), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-gray-100), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-gray-100))`
			},
			'gray-200': ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-gray-200), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-gray-200), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-gray-200))`
			},
			'gray-300': ({ opacityVariable, opacityValue }) => {
				if (opacityValue !== undefined) {
					return `hsla(var(--color-gray-300), ${opacityValue})`
				}
				if (opacityVariable !== undefined) {
					return `hsla(var(--color-gray-300), var(${opacityVariable}, 1))`
				}
				return `hsl(var(--color-gray-300))`
			},
		},
		extend: {
			fontFamily: {
				'wotfard': ['Wotfard', ...defaultTheme.fontFamily.sans],
			},
			backgroundImage: {
				'btn-blue': 'url("/images/btn-blue.svg")',
				'btn-blue-hover': 'url("/images/btn-blue-hover.svg")',
				'hero-wide': 'url("/images/pen-paper-29.jpg")',
				'hero-desktop': 'url("/images/pen-paper-28.jpg")',
				'hero-mobile': 'url("/images/pen-paper-19.jpg")',
				'try': 'url("/images/pen-paper-27.jpg")'
			},
		},
	},
	variants: {
		extend: {},
	},
	plugins: [
		require('@tailwindcss/forms'),
		require('@tailwindcss/line-clamp'),
	]
}
