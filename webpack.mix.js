const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	.styles([
        'node_modules/tiny-slider/dist/tiny-slider.css'
    ], 'public/css/vendor.css')
    .scripts([
        'node_modules/jquery/dist/jquery.js',
		'node_modules/tiny-slider/dist/tiny-slider.js'
    ], 'public/js/vendor.js')
    .js('resources/js/app.js', 'public/js/app.js')
    .postCss('resources/css/app.css', 'public/css/app.css', [
        tailwindcss('tailwind.config.js')
    ])
